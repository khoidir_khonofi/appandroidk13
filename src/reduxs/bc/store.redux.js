import { createStore, combineReducers } from 'redux'
import {
    Spinner
    
} from './reducer.redux'

const MainReducer = combineReducers({
    spinner             : Spinner
    
});

export const store = createStore(MainReducer)