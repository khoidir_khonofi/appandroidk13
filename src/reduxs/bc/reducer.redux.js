
const loading={
    loading:false
}

export const Spinner = (state = loading, action) => {
    switch (action.type) {
        case 'LOADING_TRUE':
            return {
                ...state,
                loading: true
            }
            break;
        case 'LOADING_FALSE':
            return {
                ...state,
                loading: false
            }
            break;
        default:
            break;
    }
    return state;
}
