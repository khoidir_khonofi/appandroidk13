import { createStore, combineReducers } from 'redux'
import { Auth } from '../reducers-redux/index'
import UserReducer from "../../features/UserSlice"
const MainReducer = combineReducers({
    auth: Auth,
    user: UserReducer
});

export const mainStore = createStore(MainReducer)