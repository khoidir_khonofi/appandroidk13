import * as TYPE from './../type-redux/index'

let auth={
    login:null
}

export const Auth = (state = auth, action) => {
    switch (action.type) {
        case TYPE.AUTH_LOGIN:
            return {
                ...state,
                login: action.payload
            }
            break;
        default:
            break;
    }
    return state;
}