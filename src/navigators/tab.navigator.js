import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon, useTheme } from '@ui-kitten/components';
import  UnderBuild from './../screens/underbuild-screen/index';
import Mengajar from './../screens/mengajar-screen/index';
import Home from './../screens/home-screen/home';
import Lihatnilai from '../screens/lihatnilai-screen/index';
import User from './../screens/users-screen/user';

const Tab = createBottomTabNavigator();
const IsIcon = ({ name, color }) => (
    <Icon fill={color} name={name} style={{ width: 24, height: 24, padding: 0 }} />
)

const TabNavigator = () => {
    const theme = useTheme()
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName, isColor;
                    if (route.name === 'Home') {
                        iconName = !focused ? 'home-outline' : 'home';
                        isColor = !focused ? theme['color-primary-500'] : theme['color-primary-500'];
                    } else if (route.name === 'Mengajar') {
                        iconName = !focused ? 'keypad-outline' : 'keypad';
                        isColor = !focused ? theme['color-primary-500'] : theme['color-primary-500'];
                    } else if (route.name === 'Lihatnilai') {
                        iconName = !focused ? 'keypad-outline' : 'keypad';
                        isColor = !focused ? theme['color-primary-500'] : theme['color-primary-500'];
                    } else {
                        iconName = !focused ? 'person-outline' : 'person';
                        isColor = !focused ? theme['color-primary-500'] : theme['color-primary-500'];
                    }
                    // You can return any component that you like here!
                    return (<IsIcon name={iconName} color={isColor} />);
                },
            })}
            tabBarOptions={{
                activeTintColor: theme['color-primary-500'],
                inactiveTintColor: '#9c9ea0'
            }}
        >
            <Tab.Screen
                name="Home"
                component={Home}
            />
            <Tab.Screen
                name="Lihat Nilai"
                component={Lihatnilai}
                options={{ headerShown: false }}
            />
            <Tab.Screen
                name="Mengajar"
                component={Mengajar}
            />
            <Tab.Screen
                name="User"
                component={User}
                
            />
           
        </Tab.Navigator>
    );
}

export default TabNavigator;