import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import { UnderBuild, Login } from '../screens/index';
import UnderBuild from '../screens/underbuild-screen/index';
import PrimaryNavigator from './primary.navigator';

const Stack = createStackNavigator();

export const RootNavigatorRender = ({ ...props }) => {
    return (
        <>
            <NavigationContainer>
                <Stack.Navigator>
                    {
                     1? (
                            <>
                                <Stack.Screen
                                    name="Smasrt K3"
                                    component={PrimaryNavigator}
                                    options={
                                        { headerShown: false }
                                    }
                                />
                            </>
                        ) : (
                                <>
                                    <Stack.Screen options={{ headerShown: false }} name="Login" component={UnderBuild} />
                                    <Stack.Screen options={{ headerShown: false }} name="Register" component={UnderBuild} />
                                    <Stack.Screen name="Reset Password" component={UnderBuild} />
                                </>
                            )
                    }
                </Stack.Navigator>
            </NavigationContainer>
        </>
    )
}

export default RootNavigatorRender;