import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from './../screens/home-screen/home';
import lihatsiswa from './../screens/lihatsiswa-screen/lihatsiswa';
import sikapSosial from "./../screens/sikap-screen/sikap-sosial"
import sikapSpiritual from "./../screens/sikap-screen/sikap-spiritual"
import { Icon, Button, Text, Layout, useTheme} from '@ui-kitten/components';
import TabNavigator from './tab.navigator';
import { connect } from 'react-redux'
import smpharianPengetahuan from './../screens/smp-screen/harianpengetahuan';
import smpharianKeterampilan from './../screens/smp-screen/harianketerampilan';
import smaharianPengetahuan from './../screens/sma-screen/harianpengetahuan';
import smaharianKeterampilan from './../screens/sma-screen/harianketerampilan';
import login from './../screens/site-screen/login';
import smalihatharianPengetahuan from './../screens/sma-screen/lihatharianpengetahuan';
import lihatsikapSosial from './../screens/sikap-screen/lihat-sikap-sosial';
import lihatsikapAkhir from './../screens/sikap-screen/lihat-sikap-akhir';
import lihatsikapSpiritual from './../screens/sikap-screen/lihat-sikap-spiritual';
import lihatsikapAkhirsiswa from './../screens/sikap-screen/lihat-sikap-akhir-detail';
import smalihatharianKeterampilan from './../screens/sma-screen/lihatharianketerampilan';
import smplihatharianPengetahuan from './../screens/smp-screen/lihatharianpengetahuan';
import smplihatharianKeterampilan from './../screens/smp-screen/lihatharianketerampilan';
import sdharianPengetahuan from './../screens/sd-screen/input-nilai-pengetahuan';
import sdharianKeterampilan from './../screens/sd-screen/input-nilai-keterampilan';
import sdlihatharianPengetahuan from './../screens/sd-screen/lihat-harian-pengetahuan';
import sdlihatharianKeterampilan from './../screens/sd-screen/lihat-harian-keterampilan';
import daftar from './../screens/site-screen/daftar';
import lihatnilaiperKDketerampilan from './../screens/nilai-kd-keterampilan/lihat-nilai-perkd-keterampilan';
import lihatnilaiakhir from './../screens/nilai-akhir/lihat-nilai-akhir';
import lihatnilaiakhirdetail from './../screens/nilai-akhir/lihat-nilai-akhir-detail';
import lihatnilaiperKDpengetahuan from './../screens/nilai-kd-sekolah-menengah/lihat-nilai-perkd-pengetahuan';
import lihatnilaiperKDpengetahuanakhir from './../screens/nilai-kd-sekolah-menengah/lihat-nilai-perkd-pengetahuan-akhir';
import sdlihatnilaikdpengetahuan from './../screens/sd-screen/lihat-nilai-kd-pengetahuan';
import userlihatsiswa from './../screens/users-screen/lihat-siswa';
import edituser from './../screens/users-screen/edit-form';
import inputnilainpas from './../screens/nilai-kd-sekolah-menengah/input-nilai';
import lihat from './../screens/lihatnilai-screen/lihat'
import mengajar from './../screens/mengajar-screen/mengajar'
import user from './../screens/users-screen/user'
import { AsyncStorage } from 'react-native';
import { useSelector } from 'react-redux'
import test from './../screens/users-screen/test'
import profil from './../screens/profil-screen/profil';
import * as REDUXTYPES from './../reduxs/type-redux/index'
import editsikapsosial from './../screens/sikap-screen/edit-sikap-sosial'
import editsikapspiritual from './../screens/sikap-screen/edit-sikap-spiritual'
import sdinputnilaiujian from './../screens/sd-screen/input-nilai-ujian';
import sdeditharianpengetahuan from './../screens/sd-screen/edit-harian-pengetahuan';
import editsikapakhir from './../screens/sikap-screen/edit-sikap-akhir'
import editnilaiakhir from './../screens/nilai-akhir/edit-nilai-akhir'
import smaeditharianpengetahuan from './../screens/sma-screen/edit-nilai-pengetahuan'
import smpeditharianpengetahuan from './../screens/smp-screen/edit-nilai-pengetahuan'
import smaeditharianketerampilan from './../screens/sma-screen/edit-nilai-keterampilan'
import smpeditharianketerampilan from './../screens/smp-screen/edit-nilai-keterampilan'
import sdeditharianketerampilan from './../screens/sd-screen/edit-harian-keterampilan'
import nilaiakhirkkmsatuanpendidikan from './../screens/nilai-akhir/nilai-akhir-kkm-satuan-pendidikan'
import lihatsemuakd from './../screens/nilai-kd-sekolah-menengah/lihat-semua-kd'
import lihattotalkd from './../screens/nilai-kd-sekolah-menengah/lihat-total-kd'
import sdlihatnilaisemuakd from './../screens/sd-screen/lihat-nilai-semua-kd'
import menengahnilaikkmsatuanpendidikan from './../screens/nilai-akhir/nilai-kkmsp-sekolah-menengah'
import editnilaiakhirkkmsp from './../screens/nilai-akhir/edit-nilai-akhir-kkmsp'
import lihatpengetahuanmengajar from './../screens/mengajar-screen/lihat-nilai-harian-pengetahuan'
import lihatketerampilanmengajar from './../screens/mengajar-screen/lihat-nilai-harian-keterampilan'
import lihatjurnalsikap from './../screens/sikap-screen/lihat-jurnal-sikap'
import inputjurnalsikap from './../screens/sikap-screen/input-jurnal-sikap'
import daftarsiswa from '../screens/lihatsiswa-screen/daftar-siswa'

const Stack = createStackNavigator();

const AuthStack = createStackNavigator();

const AuthScreen  = () => (
<AuthStack.Navigator>
    <AuthStack.Screen name="Login" component={login}  options={{ headerShown: false }}/>
    <AuthStack.Screen name="Daftar" component={daftar}  options={{ headerShown: false }}/>
</AuthStack.Navigator>
)

const NilaiStack = createStackNavigator();

const NilaiScreen = () => (
  <NilaiStack.Navigator>
                <NilaiStack.Screen name="Lihat" component={lihat} />
                <NilaiStack.Screen name="Lihat Sikap Sosial" component={lihatsikapSosial} />
                <NilaiStack.Screen name="Lihat Jurnal Sikap" component={lihatjurnalsikap} />
                <NilaiStack.Screen name="Lihat Sikap Spiritual" component={lihatsikapSpiritual} />
                <NilaiStack.Screen name="Lihat Sikap Akhir" component={lihatsikapAkhir} />
                <NilaiStack.Screen name="Edit Sikap Sosial" component={editsikapsosial} />
                <NilaiStack.Screen name="Edit Sikap Spiritual" component={editsikapspiritual} />
                <NilaiStack.Screen name="Edit Sikap Akhir Siswa" component={editsikapakhir} />
                <NilaiStack.Screen name="Lihat Sikap Akhir Siswa" component={lihatsikapAkhirsiswa} />
                <NilaiStack.Screen name="Edit Nilai Akhir" component={editnilaiakhir} />
                <NilaiStack.Screen name="Edit Nilai Akhir KKMSP" component={editnilaiakhirkkmsp} />
                <NilaiStack.Screen name="Sma Lihat Harian Pengetahuan" component={smalihatharianPengetahuan} options={{ headerShown: true , title : 'Nilai Harian Pengetahuan' }}/>
                <NilaiStack.Screen name="Sma Lihat Harian Keterampilan" component={smalihatharianKeterampilan} options={{ headerShown: true, title : 'Nilai Harian Keterampilan' }}/>
                <NilaiStack.Screen name="Smp Lihat Harian Pengetahuan" component={smplihatharianPengetahuan} options={{ headerShown: true , title : 'Nilai Harian Pengetahuan' }}/>
                <NilaiStack.Screen name="Smp Lihat Harian Keterampilan" component={smplihatharianKeterampilan} options={{ headerShown: true, title : 'Nilai Harian Keterampilan' }}/>
                <NilaiStack.Screen name="Sd Lihat Harian Pengetahuan" component={sdlihatharianPengetahuan} options={{ headerShown: true , title : 'Nilai Harian Pengetahuan'}}/>
                <NilaiStack.Screen name="Sd Lihat Harian Keterampilan" component={sdlihatharianKeterampilan} options={{ headerShown: true, title : 'Nilai Harian Keterampilan' }}/>
                <NilaiStack.Screen name="Lihat Nilai Perkd Pengetahuan" component={lihatnilaiperKDpengetahuan} options={{ headerShown: true }}/>
                <NilaiStack.Screen name="Lihat Nilai Perkd Pengetahuan Akhir" component={lihatnilaiperKDpengetahuanakhir} options={{ headerShown: true }}/>
                <NilaiStack.Screen name="Lihat Nilai Perkd Keterampilan" component={lihatnilaiperKDketerampilan} options={{ headerShown: true }}/>
                <NilaiStack.Screen name="Lihat Nilai Akhir" component={lihatnilaiakhir} options={{ headerShown: true }}/>
                <NilaiStack.Screen name="Lihat Nilai Akhir Detail" component={lihatnilaiakhirdetail} options={{ headerShown: true }}/>
                <NilaiStack.Screen name="Lihat Nilai Kd Pengetahuan" component={sdlihatnilaikdpengetahuan} options={{ headerShown: true }}/>
                <Stack.Screen name="Input Nilai Npas" component={inputnilainpas} options={{ headerShown: true }}/>
                <Stack.Screen name="Input Nilai Ujian" component={sdinputnilaiujian} options={{ headerShown: true }}/>  
                <Stack.Screen name="Edit Nilai Harian Pengetahuan SD" component={sdeditharianpengetahuan} options={{ headerShown: true }}/> 
                <Stack.Screen name="Edit Nilai Harian Pengetahuan SMA" component={smaeditharianpengetahuan} options={{ headerShown: true }}/>  
                <Stack.Screen name="Edit Nilai Harian Pengetahuan SMP" component={smpeditharianpengetahuan} options={{ headerShown: true }}/>  
                <Stack.Screen name="Edit Nilai Harian Keterampilan SMA" component={smaeditharianketerampilan} options={{ headerShown: true }}/>
                <Stack.Screen name="Edit Nilai Harian Keterampilan SMP" component={smpeditharianketerampilan} options={{ headerShown: true }}/> 
                <Stack.Screen name="Edit Nilai Harian Keterampilan SD" component={sdeditharianketerampilan} options={{ headerShown: true }}/>
                <Stack.Screen name="Nilai Akhir KKM Satuan Pendidikan" component={nilaiakhirkkmsatuanpendidikan} options={{ headerShown: true }}/>  
                <Stack.Screen name="Lihat Semua KD" component={lihatsemuakd} options={{ headerShown: true }}/>
                <Stack.Screen name="Lihat Total KD" component={lihattotalkd} options={{ headerShown: true }}/> 
                <Stack.Screen name="Lihat Nilai Semua KD" component={sdlihatnilaisemuakd} options={{ headerShown: true }}/>
                <Stack.Screen name="Nilai Akhir KKM Satuan Pendidikan Menengah" component={menengahnilaikkmsatuanpendidikan} options={{ headerShown: true }}/>
                <Stack.Screen name="Data Siswa" component={daftarsiswa} options={{ headerShown: true }}/>
                
  </NilaiStack.Navigator>
)


const MengajarStack = createStackNavigator();
const MengajarScreen = () => (
  <MengajarStack.Navigator>
                <MengajarStack.Screen name="Mengajar" component={mengajar}  options={{ title: 'Kelas Mengajar' }}/>
                <MengajarStack.Screen name="Siswa" component={lihatsiswa}/>
                <MengajarStack.Screen name="Sd Harian Keterampilan" component={sdharianKeterampilan} />
                <MengajarStack.Screen name="Sd Harian Pengetahuan" component={sdharianPengetahuan} />
                <MengajarStack.Screen name="Smp Harian Keterampilan" component={smpharianKeterampilan} />
                <MengajarStack.Screen name="Smp Harian Pengetahuan" component={smpharianPengetahuan} />
                <MengajarStack.Screen name="Sma Harian Keterampilan" component={smaharianKeterampilan} />
                <MengajarStack.Screen name="Sma Harian Pengetahuan" component={smaharianPengetahuan} />
                <MengajarStack.Screen name="Sikap Sosial" component={sikapSosial} />
                <MengajarStack.Screen name="Sikap Spiritual" component={sikapSpiritual} />
                <MengajarStack.Screen name="Lihat Nilai Harian Pengetahuan Mengajar" component={lihatpengetahuanmengajar} />
                <MengajarStack.Screen name="Lihat Nilai Harian Keterampilan Mengajar" component={lihatketerampilanmengajar} />
                <Stack.Screen name="Edit Nilai Harian Pengetahuan SMP" component={smpeditharianpengetahuan} options={{ headerShown: true }}/> 
                <Stack.Screen name="Edit Nilai Harian Pengetahuan SD" component={sdeditharianpengetahuan} options={{ headerShown: true }}/> 
                <Stack.Screen name="Edit Nilai Harian Pengetahuan SMA" component={smaeditharianpengetahuan} options={{ headerShown: true }}/>  
                <Stack.Screen name="Edit Nilai Harian Keterampilan SMA" component={smaeditharianketerampilan} options={{ headerShown: true }}/>
                <Stack.Screen name="Edit Nilai Harian Keterampilan SMP" component={smpeditharianketerampilan} options={{ headerShown: true }}/> 
                <Stack.Screen name="Edit Nilai Harian Keterampilan SD" component={sdeditharianketerampilan} options={{ headerShown: true }}/>
                <NilaiStack.Screen name="Lihat Nilai Perkd Pengetahuan" component={lihatnilaiperKDpengetahuan} options={{ headerShown: true }}/>
                <NilaiStack.Screen name="Lihat Nilai Kd Pengetahuan" component={sdlihatnilaikdpengetahuan} options={{ headerShown: true }}/>
                <NilaiStack.Screen name="Lihat Nilai Perkd Keterampilan" component={lihatnilaiperKDketerampilan} options={{ headerShown: true }}/>
                <NilaiStack.Screen name="Input Jurnal Sikap" component={inputjurnalsikap} options={({route}) => ({title : route.params.id})}/>
  </MengajarStack.Navigator>
)

const UserStack = createStackNavigator();
const UserScreen = () => (
  <UserStack.Navigator>
          <UserStack.Screen name="User" component={user} />
          <UserStack.Screen name="Edit User" component={edituser} />
  </UserStack.Navigator>
)

const LogoutIcon = (props) => (
  <Icon {...props} name='log-out-outline' fill = '#36beff'/>
);

const ProfilStack = createStackNavigator();
const ProfilScreen = () => (
  <ProfilStack.Navigator>
          <ProfilStack.Screen name="Profil" component={profil}  options={{
          title: 'My profil',
          headerRight: (setAuth) => (
            <Button
              onPress={() => {AsyncStorage.clear(); setAuth(null)}}
              appearance='ghost'
              accessoryLeft={LogoutIcon}
            ></Button>
          ),
        }}/>
  </ProfilStack.Navigator>
)



const BottomTab = createBottomTabNavigator();
const IsIcon = ({ name, color }) => (
  <Icon fill={color} name={name} style={{ width: 24, height: 24, padding: 0 }} />
)
const HomeScreen = () => {
  const theme = useTheme()
  return (
    <BottomTab.Navigator
          screenOptions={({ route }) => ({
              tabBarIcon: ({ focused, color, size }) => {
                  let iconName, isColor;
                  if (route.name === 'Home') {
                      iconName = !focused ? 'home-outline' : 'home';
                      isColor = !focused ? color: '#36beff', theme['color-primary-500'];
                  } else if (route.name === 'Mengajar') {
                      iconName = !focused ? 'keypad-outline' : 'keypad';
                      isColor = !focused ? color: '#36beff', theme['color-primary-500'];
                  } else if (route.name === 'Lihat Nilai') {
                      iconName = !focused ? 'eye-outline' : 'eye';
                      isColor = !focused ? color: '#36beff', theme['color-primary-500'];
                  }else if (route.name === 'Profil') {
                        iconName = !focused ? 'person-outline' : 'person';
                        isColor = !focused ? color: '#36beff', theme['color-primary-500'];
                  } else {
                      iconName = !focused ? 'list-outline' : 'list';
                      isColor = !focused ? color: '#36beff', theme['color-primary-500'];
                  }
                  // You can return any component that you like here!
                  return (<IsIcon name={iconName} color={isColor} />);
              },
          })}
          tabBarOptions={{
              activeTintColor: 'blue',
              inactiveTintColor: '#36beff'
          }}
      >
        
        <BottomTab.Screen name='Home' component={Home} options={{headerShown : true}}/>
        <BottomTab.Screen name='Lihat Nilai' component={NilaiScreen} />
        <BottomTab.Screen name='Mengajar' component={MengajarScreen} />
        <BottomTab.Screen name='User' component={UserScreen} />
        <BottomTab.Screen name='Profil' component={ProfilScreen}/>
    </BottomTab.Navigator>
)
        }


const RenderPrimaryNavigator = ({ navigation, auth, setAuth, ...props }) => {
  React.useEffect(()=>{
    (async function(){
      let result = await AsyncStorage.getItem("auth")
      if(result){
        setAuth(result)
      }
    })()
  },[])
  const counter = useSelector(state => state.counter)
    return (
        <NavigationContainer>
            <Stack.Navigator>
              {
                auth.login?(
                  <Stack.Screen name={'HomeScreen'} component={HomeScreen} options = {{ headerShown : false}}/>
                ):(
                  <Stack.Screen name={'AuthScreen'} component={AuthScreen} options={{ headerShown:   false }}/>
                )
              }
              {/* <Stack.Screen name={'HomeScreen'} component={HomeScreen} options={{ headerShown: false }}/> */}
            </Stack.Navigator>
        </NavigationContainer>
    );
}

// conect to redux
const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RenderPrimaryNavigator);

