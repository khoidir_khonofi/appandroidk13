import React, { useState, useEffect } from 'react';
import { StyleSheet, Picker, ActivityIndicator, View} from 'react-native';
import { Layout, Text , Avatar, MenuItem,
   Card, Modal,Icon,Button, Spinner } from '@ui-kitten/components';
import axios from 'axios';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';

const List =({data, nav, role, saveDatasd, saveData, jenjangpendidikan, isSave})=>{
  const [visible, setVisible] = React.useState(false);
  const [idClick, setIdClick] = useState(0);
  // const role = useSelector(selectRole)

  const indikator = (props) => {
    return isSave ? (
     <View style={[props.style, styles.indicator]}>
         <Spinner size='large'/>
     </View>                     
       ) : null
   }

  

  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaiakhir/delete&id="+idClick).then((respon) => {
        alert(respon.data);
    }).catch((e) => console.log(e))
  }
    return(
      <Layout style={{marginHorizontal:5, backgroundColor:'transparent', borderRadius:5}}>
        {role === 'Administrator' || role === 'Guru' ? (
            <>
              {jenjangpendidikan === 'SD' ? (
                <Layout style={{marginRight: 30, marginLeft: 30, marginTop: 20}} level='1'>
                    <Button onPress={() => saveDatasd()}   appearance='outline' status='info' accessoryLeft={indikator}>
                    kkm satuan pendidikan</Button>
                </Layout>
              ):(
                <Layout style={{marginRight: 30, marginLeft: 30, marginTop: 20}} level='1'>
                    <Button onPress={() => saveData()}   appearance='outline' status='info' accessoryLeft={indikator}>
                    kkm satuan pendidikan</Button>
                </Layout>
              )}
            </>
        ):null}
        
         
        <FlatList
          data={data}
          keyExtractor={(item, index) => {item.id_nilai}}
          renderItem={({item})=>{
            return(
              <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}}>

                
                
                  <Layout style={{flexDirection:'row'}}>
                      
                      <Layout style={{flex:3, marginLeft:5, padding:5}}>
                        <Text>Nama : {item.nama_siswa}</Text>
                        <Text>Mata Pelajaran : {item.nama_mata_pelajaran}</Text>
                        <Text>Nilai Pengetahuan : {item.nilai_pengetahuan}</Text>
                        <Text>Predikat Pengetahun : {item.predikat_pengetahuan}</Text>
                        <Text>Deskripsi Pengetahuan : {item.deskripsi_pengetahuan}</Text>
                        <Text>Nilai Keterampilan : {item.nilai_keterampilan}</Text>
                        <Text>Predikat Keterampilan : {item.predikat_keterampilan}</Text>
                        <Text>Deskripsi Keterampilan : {item.deskripsi_keterampilan}</Text>
                      </Layout>
                      {role === 'Administrator' || role === 'Guru' ? (
                        <>
                      <Layout
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                        >
                        <Icon name="more-vertical-outline" fill='#8F9BB3' onPress={() => {
                          setVisible(true);
                          setIdClick(item.id_nilai);
                          
                        }} style={{  width: 32,
                          height: 32}}/>
                      </Layout>
                      </>
                      ):null}
                  </Layout>
              </Layout>
            )
          }}
        />
        <Modal 
          visible={visible} 
          backdropStyle={styles.backdrop}
          style={styles.modall}
          onBackdropPress={() => setVisible(false)}>
           
              
              <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Edit Nilai Akhir", { id: idClick })
                          
                        }
                        }
                        
                        title={"Edit "}
                      />

                      <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          Delete({ id: idClick })
                          }
                        }
                        
                        title="Delete"
                      />
             
        </Modal>
      </Layout>
    )
  }
  
  function App({navigation, route, auth}) {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    const [isSave, setisSave] = useState(false)
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
      username:null, role:null,
      email: null,
      token:null};

    
    const [jenjangpendidikan, setstate] = useState("")
    
    //check rolw
    const getApi=()=>{
        axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaiakhir&token=`+token).then((result) => {
         
          setData(result.data);
          setIsLoading(true)
        }).catch((e)=> alert('Data Kosong'))

        axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/user/sekolah&token=${token}`)
        .then((res) => setstate(res.data)).catch((e) => alert(e))
        
      }
   
    useEffect(() => {
      getApi();
    },[]);
   
    const saveDatasd = () => {
      setisSave(true)
      axios.post("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaiakhir/nilaiakhirkkmsatuanpendidikan&token="+token).then((result) => {
         
      setData(result.data);
      alert('sukses')
      setisSave(false)
      navigation.navigate('Nilai Akhir KKM Satuan Pendidikan')
    }).catch((e)=> alert(e))
    
  }

  const saveData = () => {
    setisSave(true)
    axios.post("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaiakhir/nilaiakhirkkmsatuanpendidikansekolahmenengah&token="+token).then((result) => {
       
    setData(result.data);
    alert('sukses')
    setisSave(false)
    navigation.navigate('Nilai Akhir KKM Satuan Pendidikan')
  }).catch((e)=> alert(e))
  
}

    return isLoading ? (
      <>
        <List role={role} saveDatasd={saveDatasd} nav={navigation} data={data} jenjangpendidikan={jenjangpendidikan}saveData={saveData} isSave={isSave}/>
      </>
    ) : (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )
  }
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
    modall: {
      width:200,
      backgroundColor:"#1bcaff",
      color :"red",
      borderRadius : 5,
    },
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.7)',
    }
  });


// conect to redux
const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
