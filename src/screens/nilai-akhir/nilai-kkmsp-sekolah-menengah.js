import React, { useState, useEffect } from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { Layout, Text , Avatar, MenuItem,
  Button, Card, Modal,Icon} from '@ui-kitten/components';
import axios from 'axios';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import {useSelector} from "react-redux";
import {selectRole, selectToken} from "../../features/UserSlice";

const List =({data, nav})=>{
  const [visible, setVisible] = React.useState(false);
  const [idClick, setIdClick] = useState(0);
  const role = useSelector(selectRole);
  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaiakhir/delete&id="+idClick).then((respon) => {
        alert(respon.data);
    }).catch((e) => console.log(e))
  }
    return(
      <Layout style={{marginHorizontal:5, backgroundColor:'transparent', borderRadius:5}}>
        <FlatList
          data={data}
          keyExtractor={(item, index) => item.id_nilai}
          renderItem={({item})=>{
            return(
              <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}}>
                
                  <Layout style={{flexDirection:'row'}}>
                      
                      <Layout style={{flex:1, marginLeft:16, padding:5}}>
                        <Text>Nama : {item.siswa.nama}</Text>
                        <Text>Mata Pelajaran : {item.mapel.nama_mata_pelajaran}</Text>
                        <Text>Nilai Pengetahuan : {item.nilai_pengetahuan}</Text>
                        <Text>Predikat Pengetahun : {item.predikat_pengetahuan}</Text>
                        <Text>Deskripsi Pengetahuan : {item.deskripsi_pengetahuan}</Text>
                        <Text>Nilai Keterampilan : {item.nilai_keterampilan}</Text>
                        <Text>Predikat Keterampilan : {item.predikat_keterampilan}</Text>
                        <Text>Deskripsi Keterampilanere : {item.deskripsi_keterampilan}</Text>
                      </Layout>
                      <Layout
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                        >
                        <Icon name="more-vertical-outline" fill='#8F9BB3' onPress={() => {
                          setVisible(true);
                          setIdClick(item.id_nilai);
                          
                          
                        }} style={{  width: 32,
                          height: 32}}/>
                      </Layout>
                  </Layout>
              </Layout>
            )
          }}
        />
        <Modal 
          visible={visible} 
          backdropStyle={styles.backdrop}
          style={styles.modall}
          onBackdropPress={() => setVisible(false)}>
                      <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Edit Nilai Akhir", { id: idClick })
                          
                        }
                        }
                        
                        title={"Edit "+idClick}
                      />

                      <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          Delete({ id: idClick })
                          }
                        }
                        
                        title="Delete"
                      />
        </Modal>
      </Layout>
    )
  }
  
  function App({navigation, route}) {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    const token = useSelector(selectToken);
    //check rolw
    const getApi=()=>{
        axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaiakhir/nilaiakhirkkmsatuanpendidikansekolahmenengah&token="+token).then((result) => {
         
          setData(result.data);
          setIsLoading(true)
        }).catch((e)=> alert(e))
        
      }
   
    useEffect(() => {
      getApi();
    },[]);
   
    return isLoading ? (
      <>
        <List nav={navigation} data={data} />
      </>
    ) : (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )
  }
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
    modall: {
      width:200,
      backgroundColor:"#1bcaff",
      color :"red",
      borderRadius : 5,
    },
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.7)',
    }
  });

export default App;