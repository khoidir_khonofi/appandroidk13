import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, Button, View } from 'react-native';
import { IndexPath, Layout, Input, Text } from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation}) {
  const [isLoad, setIsLoad] = useState(false)
  const [data, setData] = useState([]);
  const [isSave, setisSave] = useState(false)
  const [deskripsipengetahuan, setDeskripsiPengetahuan] = useState("");
  const [deskripsiketerampilan, setDeskripsiKeterampilan] = useState("");
  
  

  useEffect(() => {
    setIsLoad(true)
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaiakhir/selectkkmsp&id="+route.params.id).then((result) => {
        setDeskripsiPengetahuan(result.data.deskripsi_pengetahuan)
        setDeskripsiKeterampilan(result.data.deskripsi_keterampilan)
      setIsLoad(false)
    }).catch((e) => console.warn(e))
  },[])

  const saveData = () => {
    setisSave(true)
    axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaiakhir/updatekkmsp&id="+route.params.id, {
      id_nilai: route.params.id,
      deskripsi_pengetahuan: deskripsipengetahuan,
      deskripsi_keterampilan: deskripsiketerampilan,
    

    }).then((res) => {
        setisSave(false)
      alert(res.data);
  }).catch((e) => {alert('erros nyimpan'); setisSave(false)})
  }

  return (
      <>
       

<Layout style={styles.container} level='1'>
    <Layout style={{ paddingRight: 30, paddingLeft: 30}}>
            {isSave ? (
                <View style={[styles.container, styles.horizontal]}>
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
            ) : null} 
            {isLoad ? (
                <View style={[styles.container, styles.horizontal]}>
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
            ) : null} 
        <Layout>
          <Input 
            placeholder='Keterangan Sosial'
            multiline = {true}
            value={deskripsipengetahuan}
            onChangeText={nextValue => setDeskripsiPengetahuan(nextValue)}
          /> 
        </Layout>

        <Layout>
          <Input 
            placeholder= 'Keterangan Spiritual'
            multiline = {true}
            value={deskripsiketerampilan}
            onChangeText={nextValue => setDeskripsiKeterampilan(nextValue)}
          /> 
        </Layout>
        <Layout>
            <Button onPress={() => saveData()} title="Simpan"/>
        </Layout>
  </Layout>
</Layout>
  
        
            
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});