import React, {useState, useEffect} from 'react';
import { Layout, Text,Button, Icon } from '@ui-kitten/components';
import { AsyncStorage } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { TouchableOpacity, FlatList} from 'react-native-gesture-handler';
import axios from 'axios'

import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';

function Home({auth, setAuth, ...props}){
  const [data, setData] = useState([])
  let token = auth.login?JSON.parse(auth.login): {
    username:null, role:null,
    email: null,
    token:null};
  // console.log(auth.login)
 
  const LogoutIcon = (props) => (
    <Icon {...props} name='log-in-outline'/>
  );

  
  // let a = JSON.stringify(username.username)
 
  return (
    <>
   
      <Layout style={{justifyContent : 'center', alignItems: 'center', flex: 1}}>
        <Text style={{textAlign:'center', fontSize: 25}} status='info'>SMART-K13</Text>  
        <Text style={{textAlign:'center', textTransform:'capitalize', fontSize:20, marginBottom:20}}>APLIKASI  PENILAIAN KURIKULUM TAHUN 2013 BERBASIS ANDROID BERDASARKAN PERMENDIKBUD NOMOR 24 TAHUN 2016
        </Text>
        <Button  onPress={() => {AsyncStorage.clear(); setAuth(null)}} status='info' accessoryLeft={LogoutIcon}> {'Logout '+token.username}  </Button>
        
      </Layout>    
    </>
  )
  
}

// conect to redux
const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
