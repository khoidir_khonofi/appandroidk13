import {Layout, Text, Avatar, Icon} from '@ui-kitten/components';
import React, {useState, useEffect, useRef} from 'react';
import axios from 'axios';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import {TouchableOpacity, FlatList} from 'react-native-gesture-handler';
import RBSheet from 'react-native-raw-bottom-sheet';

// const dataOptions = ['Nilai Keterampilan', 'Nilai Pengetahuan'];

export default function App({route, navigation}) {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const refRBSheet = useRef();
  const [idSiswa, setIdSiswa] = useState(0);

  const getApi = async () => {
    let result = await axios(
      'http://192.168.43.13/tugasakhir/web/index.php?r=api/user/lihatsiswa&id=' +
        route.params.id_user,
    );

    if (result) {
      setIsLoading(true);

      setData(result.data);
    }
  };

  useEffect(() => {
    (async function run() {
      await getApi();
    })();
  }, []);

  return isLoading ? (
    <Layout style={{marginHorizontal: 5, backgroundColor: 'transparent'}}>
      <FlatList
        data={data}
        keyExtractor={(item, index) => item.id_siswa}
        renderItem={({item}) => {
          return (
            <Layout
              style={{margin: 5, backgroundColor: '#1bcaff', borderRadius: 15}}>
              <Layout>
                <Layout style={{flexDirection: 'row'}}>
                  <Layout style={{padding: 5}}>
                    <Avatar
                      resizeMode={'cover'}
                      source={{uri: item.siswa.foto}}
                    />
                  </Layout>
                  <Layout
                    style={{
                      flex: 1,
                      marginLeft: 16,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text>
                      {item.siswa.nama}({item.siswa.nis})
                    </Text>
                    <Text></Text>
                  </Layout>
                  <Layout
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'flex-end',
                      alignItems: 'center',
                      paddingRight: 2,
                    }}>
                    <>
                      <TouchableOpacity
                        onPress={() => {
                          refRBSheet.current.open();
                          setIdSiswa(item.siswa.id);
                        }}>
                        <Icon
                          style={{width: 32, height: 32}}
                          fill="#8F9BB3"
                          name="more-vertical-outline"
                        />
                      </TouchableOpacity>
                    </>
                  </Layout>
                </Layout>
              </Layout>
            </Layout>
          );
        }}
      />
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          draggableIcon: {
            backgroundColor: '#000',
          },
        }}>
        <Layout>
        <Text
          onPress={() => {refRBSheet.current.close(); navigation.navigate('Sd Harian Keterampilan',{id: idSiswa})}}
            style={{
              marginBottom : 10,
              color : 'white',
              padding: 10,
              fontSize: 18,
              backgroundColor: 'grey',
              textAlign: 'center',
              height: 50,
              borderBottomWidth: 1,
              borderColor: 'grey',
            }}>
            Edit
          </Text>
          
        </Layout>
      </RBSheet>
    </Layout>
  ) : (
    <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="large" color="#0000ff" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});
