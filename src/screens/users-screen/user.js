import React, { useState, useEffect } from 'react';
import { StyleSheet, ActivityIndicator, View, RefreshControl, SafeAreaView, ScrollView,} from 'react-native';
import { Icon, Layout, Text , Avatar,MenuItem, Button, Card, Modal } from '@ui-kitten/components';
import axios from 'axios';
import { connect } from 'react-redux'
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import CircleButton from 'react-native-circle-button';


const List = ({data,Konfirmasi, Aktif, Delete, setIdClick, idClick, role, nav}) => {
  
  const [visible, setVisible] = React.useState(false);
  const [name, setName] = useState("");
  const [status, setStatus] = useState("");
  return (
    <ScrollView>
    <Layout style={{backgroundColor:'transparent'}} level='1'>
          {data.map((item) => {
          return(
            
            <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}} level='2'>
                <Layout style={{flexDirection: 'row'}} level='1'>
                    <Layout style={{flex: 3,marginLeft:5}} level='1'>
                      <Text>Email : {item.email}</Text>
                      <Text>Role : {item.role}</Text>
                      <Text>Username : {item.username}</Text>
                      <Text>Status : {item.status}</Text>
                    </Layout>
                    {
                      role === "Guru" || role === "Administrator" ? (
                        <Layout
                          style={{
                            flex: 1,
                            flexDirection: "row",
                            justifyContent: "flex-end",
                            alignItems: "center",
                          }}
                          level='1'
                        >
                        <Icon name="more-vertical-outline" fill='green' onPress={() => {
                          setVisible(true);
                          setIdClick(item.id_user);
                          setName(item.username);
                          setStatus(item.status);
              
                        }} style={{  width: 32,
                          height: 32}}/>
                    </Layout>
                      ): null
                    }
                </Layout>
            </Layout> 
          );
        })}
       <Modal 
          style = {{width: 200, borderRadius:5, backgroundColor:"#1bcaff"}}
                      visible={visible} 
                      backdropStyle={styles.backdrop}
                      onBackdropPress={() => setVisible(false)}>
                                  <MenuItem onPress={() =>{ setVisible(false); nav.navigate("Edit User", { id: idClick })
                                    }
                                    }
                                    title="Edit"
                                  />
                                  {status === 'Aktif' ? (
                                    <MenuItem
                                      onPress={() =>{
                                        setVisible(false)
                                        Konfirmasi({ id: idClick })
                                        }
                                      }
                                      title={"Nonaktifkan "+name}
                                    />
                                  ) : (
                                    <MenuItem
                                        onPress={() =>{
                                          setVisible(false)
                                          Aktif({ id: idClick })
                                          }
                                        }
                                        title={"Aktifkan "+name}
                                      />
                                      )}
                                  
                                  <MenuItem
                                    onPress={() =>{
                                      setVisible(false)
                                      Delete({ id: idClick })
                                      }
                                    }
                                    title="Delete"
                                  />
        </Modal>
    </Layout>
    </ScrollView>
  )
}

function AppRender({navigation, route, auth}) {
  
  const [data, setData] = useState([]);
  const [idClick, setIdClick] = useState(0);
  let {token, email, role} = auth.login?JSON.parse(auth.login): {
    username:null, role:null,
    email: null,
    token:null};
  const [isLoading, setIsLoading] = useState(false)
  const getApi=()=>{
    setIsLoading(true)
      axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/user&token="+token).then((result) => {
       
        setData(result.data);
        
        setIsLoading(false)
      }).catch((e)=> alert("Error"+e))
      
    }
 
  useEffect(() => {
    getApi();
  },[]);

 

  const Konfirmasi = () => {
    setIsLoading(true)
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/user/tidakaktif&id="+idClick,{
    }).then((res) => {
     setIsLoading(false) 
      alert(res.data);
  }).catch((e) => {console.log('error', e)})
  }

  const Aktif = () => {
    setIsLoading(true)
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/user/aktif&id="+idClick,{
    }).then((res) => {
      setIsLoading(false) 
      alert(res.data);
  }).catch((e) => {console.log('error', e)})
  }

  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/user/delete&id="+idClick,{
    }).then((res) => {
      alert(res.data);
  }).catch((e) => {console.log('error hapus', e)})
  }

   return !isLoading  ? (
      <List data={data} Konfirmasi={Konfirmasi} Aktif={Aktif} Delete={Delete} setIdClick={setIdClick} idClick={idClick} role={role} nav={navigation}/>
    
   ):(
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
   )
}
  
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});

// conect to redux
const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppRender);
  