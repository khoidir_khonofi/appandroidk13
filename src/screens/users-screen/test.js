import React, {useEffect, useState} from 'react'
import {View, Text, Button, FlatList} from 'react-native'
import axios from 'axios'

const List = ({data, route, nav}) => {
    

    return(
        <FlatList
        data = {data}
        renderItem = {({item}) => {
            return (
                <View>
                    <Text>{item.username}</Text>
                </View>
            )
        }
        }
        />
    )
}

function App({route, navigation}){
    const [data, setData] = useState([])
    const [isloading, setIsloading] = useState(false)
    const getApi=()=>{
        //http://localhost/projectpabw/pelanggan.php
        axios.get("http://192.168.43.13/projectpabw/pelanggan.php").then((result) => {
         
          setData(result.data);
          setIsloading(true)
        }).catch((e)=> alert("Error"))
        
      }
   
    useEffect(() => {
      getApi();
    },[]);
    return isloading ? (
        <List nav = {navigation} data={data}/>
    ): (
        <Text>loading...</Text>
    )
}
export default App
