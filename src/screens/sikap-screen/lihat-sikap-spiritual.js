import React, { useState, useEffect } from 'react';
import { StyleSheet, Picker, ActivityIndicator,View } from 'react-native';
import { Layout, Text , Avatar,MenuItem, Button, Card, Modal, Icon} from '@ui-kitten/components';
import axios from 'axios';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';
import {
  MenuProvider,
  Menu,
  MenuTrigger,
  MenuOptions,
  MenuOption,
} from 'react-native-popup-menu';

Menu.debug = true;

const List =({data, nav, role})=>{
  const [isMenuOpen, setisMenuOpen] = useState(false)
  const [idClick, setIdClick] = useState(0);
  const [idSiswa, setIdSiswa] = useState(0);
  const [name, setName] = useState("");
  const [visible, setVisible] = React.useState(false);
  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/deletespiritual&id="+idClick,{
    }).then((res) => {
      alert(res.data);
  }).catch((e) => {console.log('error hapus', e)})
  }
    return(
      
      <Layout style={{marginHorizontal:5, backgroundColor:'transparent', borderRadius:5}}>
        
        <FlatList
          data={data}
          keyExtractor={(item, index) => item.id_sikap_spiritual}
          renderItem={({item})=>{
            return(
              <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}}>
                  <Layout style={{flexDirection:'row'}}>
                      
                      <Layout style={{flex:3, marginLeft:3, padding:5}}>
                        <Text>{item.nama_siswa} {item.tingkat_kelas}{item.nama_kelas}</Text>
                        <Text>Butir Sikap : {item.butir_sikap} </Text>
                        <Text>Nilai : {item.nilai}</Text>
                      </Layout>
                      <Layout
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                        >
                          <Icon name="more-vertical-outline" fill='#8F9BB3' onPress={() => {
                          setVisible(true);
                          setIdClick(item.id_sikap_spiritual);
                          setIdSiswa(item.id_siswa)
                          
                          
                        }} style={{  width: 32,
                          height: 32}}/>
                      </Layout>
                  </Layout>
              </Layout>
            )
          }}
        />
        
        <Modal 
                        style = {{width: 200, backgroundColor:"#1bcaff"}}
                        backdropStyle={styles.backdrop}
                        visible={visible} 
                        onBackdropPress={() => setVisible(false)}>
                            {role === 'Administrator' || role === 'Guru' ? (
                              <>
                               <MenuItem onPress={() =>{ setVisible(false); nav.navigate("Edit Sikap Spiritual", { id: idClick })
                              }
                              }
                              title="Edit"
                            />
                             
                            
                            <MenuItem
                              onPress={() =>{
                                setVisible(false)
                                Delete({ id: idClick })
                                }
                              }
                              title="Delete"
                            />

                            <MenuItem
                             onPress={() =>{ setVisible(false); nav.navigate('Lihat Sikap Akhir Siswa',{id_siswa: idSiswa})
                            }
                            }
                              title={"Lihat Sikap Akhir "}
                            />
                            </>
                            ):(
                              <MenuItem
                             onPress={() =>{ setVisible(false); nav.navigate('Lihat Sikap Akhir Siswa',{id_siswa: idSiswa})
                            }
                            }
                              title={"Lihat Sikap Akhir "}
                            />
                            )}
                                   
                      </Modal>
      </Layout>
      
    )
  }
  
  function App({navigation, auth}) {
    const [data, setData] = useState([]);
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
      username:null, role:null,
      email: null,
      token:null};
    const [isLoading, setIsLoading] = useState(false)
    const getApi=()=>{
      axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/sikapspiritual&token="+token).then((result) => {
       
        setData(result.data);
        setIsLoading(true)
      }).catch((e)=> alert("kosong"))
      
    }
   
    useEffect(() => {
      getApi();
    },[]);
   
    return isLoading ? (
      <>
        <List nav={navigation} data={data} role = {role}/>
      </>
    ) : (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )
  }
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      paddingTop: 20
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
  });

  const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(App);