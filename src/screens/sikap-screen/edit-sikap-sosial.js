import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View, AsyncStorage } from 'react-native';
import { IndexPath, Layout, Input, Text, Button, Spinner } from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation}) {
  const [isLoad, setIsLoad] = useState(false)
  const [data, setData] = useState([]);
  const [isSave, setisSave] = useState(false)
  
  
  const [nilai, setNilai] = useState("")
  const [nama, setNama] = useState("")
  const [butirSikap, setButirSikap] = useState("")

  const indikator = (props) => {
    return isSave ? (
     <View style={[props.style, styles.indicator]}>
         <Spinner size='large'/>
     </View>                     
       ) : null
   }

  useEffect(() => {
    setIsLoad(true)
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/butirsikap&id="+route.params.id).then((result) => {
        setData(result.data)
        setNilai(result.data.data.nilai.toString())
        setNama(result.data.data.nama)
        setButirSikap(result.data.data.butir_sikap)
        setIsLoad(false)
    }).catch((e) => console.warn(e))
  },[])

  const saveData = () => {
    setisSave(true)
    axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/update&id="+route.params.id, {
      id_sikap_sosial: route.params.id,
      butir_sikap: butirSikap,
      nilai: nilai,
    }).then((res) => {
        setisSave(false)
        alert(res.data);
  }).catch((e) => {alert(e); setisSave(false)})
  }
 let a = 10;
  return (
      <>
        
        <Layout style={styles.container} level='1'>
       
           {isLoad ? (
               <View style={[styles.container, styles.horizontal]}>
               <ActivityIndicator size="large" color="#0000ff" />
            </View>
          ) : null} 
            <Layout style={{paddingRight: 30, paddingLeft: 30}}>
              <Text style={{marginBottom: 20, textAlign: 'center'}}>Edit sikap {nama}</Text>
                <Layout style={{backgroundColor: "#f4f5f7", marginBottom :10}}>
                    <Picker
                    selectedValue={butirSikap}
                    style={{height: 50, width: 300}}
                    onValueChange={(itemValue, itemIndex) => {
                        setButirSikap(itemValue)}
                    }>
                        <Picker.Item label="Jujur" value="Jujur"/>
                        <Picker.Item label="Disiplin" value="Disiplin"/>
                        <Picker.Item label="Tanggung Jawab" value="Tanggung Jawab"/>
                        <Picker.Item label="Santun" value="Santun"/>
                        <Picker.Item label="Peduli" value="Peduli"/>
                        <Picker.Item label="Percaya Diri" value="Percaya Diri"/>
                    </Picker>
                </Layout>
                <Layout>
                <Input 
                    placeholder='Nilai'
                    keyboardType = 'numeric'
                    value={nilai}
                    onChangeText={nextValue => setNilai(nextValue)}
                /> 
                </Layout>
           
        </Layout>
        <Layout style={{paddingRight: 30, paddingLeft: 30, marginTop :10}}>
            <Button onPress={() => saveData()} status='info'  appearance='outline' accessoryLeft={indikator}>
            Simpan
            </Button>
            </Layout>
</Layout>
  
       
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});