import React, { useState, useEffect } from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { Layout, Text , Avatar, MenuItem,
  Button, Card, Modal,Icon} from '@ui-kitten/components';
import axios from 'axios';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';

const List =({data, nav, role})=>{
  const [visible, setVisible] = React.useState(false);
  const [isopen, setIsopen] = useState(false)
  const [idClick, setIdClick] = useState(0);
  const [idsiswa, setidSiswa] = useState(0)

  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/deletesikapakhir&id="+idClick).then((respon) => {
        alert(respon.data);
    }).catch((e) => console.log(e))
  }

  const Load = () => {
    axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/load&id="+idsiswa).then((respon) => {
        alert(respon.data);
    }).catch((e) => console.log(e))
  }


    return(
      <Layout style={{marginHorizontal:5, backgroundColor:'transparent', borderRadius:5}}>
        
        <FlatList
          data={data}
          keyExtractor={(item, index) => item.key}
          renderItem={({item})=>{
            return(
              <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}}>
                  <Layout style={{flexDirection:'row'}}>
                      
                      <Layout style={{flex:5, marginLeft:3, padding:5}}>
                        <Text>{item.nama_siswa} - {item.tingkat_kelas}{item.nama_kelas}</Text>
                        <Text>Keterangan Sosial : {item.keterangan_sosial}</Text>
                        <Text>Keterangan Spiritual : {item.keterangan_spiritual}</Text>
                      </Layout>
                      {role === 'Administrator' || role === 'Guru' ? (
                        <>
                      <Layout
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                        >
                        <Icon name="more-vertical-outline" fill='#8F9BB3' onPress={() => {
                          setVisible(true);
                          setIdClick(item.id_sikap);
                          setidSiswa(item.id_siswa)
                          
                          
                        }} style={{  width: 32,
                          height: 32}}/>
                      </Layout>
                      </>
                      ):null}
                  </Layout>
              </Layout>
            )
          }}
        />
        <Modal 
          visible={visible} 
          backdropStyle={styles.backdrop}
          style={styles.modall}
          onBackdropPress={() => setVisible(false)}>
              <MenuItem
              onPress={() =>{
                setVisible(false)
                nav.navigate("Edit Sikap Akhir Siswa", { id: idClick })
                
              }
              }
              
              title={"Edit "}
            />

            <MenuItem
              onPress={() =>{
                setVisible(false)
                Delete({ id: idClick })
                }
              }
              
              title="Delete"
            />

            <MenuItem
              onPress={() =>{
                setVisible(false)
                Load({ id: idsiswa })
                }
              }
              
              title="Load Jurnal Sikap"
            />
                      
        </Modal>
      </Layout>
    )
  }
  
  function App({navigation, auth}) {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
      username:null, role:null,
      email: null,
      token:null};
    const getApi=()=>{
      axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/sikapakhir&token="+token).then((result) => {
       
        setData(result.data);
        setIsLoading(true)
      }).catch((e)=> alert("data kosong"))
      
    }
   
    useEffect(() => {
      getApi();
    },[]);
   
    return isLoading ? (
      <>
        <List nav={navigation} data={data} role={role}/>
      </>
    ) : (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )
  }
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
    modall: {
      width:200,
      backgroundColor:"#1bcaff",
      color :"red",
      borderRadius : 5,
    },
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.7)',
    }
  });

  const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(App);