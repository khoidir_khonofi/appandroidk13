import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { IndexPath, Layout, Input, Text, Button } from '@ui-kitten/components';
import axios from "axios";

function App({route}) {
    const [issave, setisSave] = useState(false)
    const [sikap, setSikap] = useState("");
    const [butirsikap, setButirsikap] = useState("");
    const [catatanperilaku, setCatatanperilaku] = useState("");
    const [penilaian, setPenilaian] = useState("");
    const [tindaklanjut, setTindaklanjut] = useState("")

    const saveData = () => {
        setisSave(true)
        axios.post("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/postjurnalsikap&id="+route.params.id, {
          id_siswa: route.params.id,
          sikap: sikap,
          butir_sikap: butirsikap,
          catatan_perilaku: catatanperilaku,
          penilaian: penilaian,
          tindak_lanjut: tindaklanjut, 
        
    
        }).then((res) => {
        setisSave(false)
          alert(res.data);
        }).catch((e) => {alert(e); setisSave(false)})

    }

    return (
      <Layout style={styles.container} level='1'>
        {issave ? (
            <View style={[styles.container, styles.horizontal]}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>
        ):null}
          <Layout style={{backgroundColor: '#f4f5f7', marginLeft:30, marginRight:30, marginBottom:5}}>
              <Picker
                    selectedValue={sikap}
                    style={{height: 40, width: 300}}
                    onValueChange={(itemValue, itemIndex) =>
                      setSikap(itemValue)
                    }>
                        <Picker.Item label="Sosial" value="Sosial"/>
                        <Picker.Item label="Spiritual" value="Spritual"/>
              </Picker>
          </Layout>
          <Layout style={{backgroundColor: '#f4f5f7', marginLeft:30, marginRight:30, marginBottom:5}}>
            {sikap === 'Sosial' ? (
                 <Picker
                    selectedValue={butirsikap}
                    style={{height: 40, width: 300}}
                    onValueChange={(itemValue, itemIndex) =>
                      setButirsikap(itemValue)
                    }>
                        <Picker.Item label="Jujur" value="Jujur"/>
                        <Picker.Item label="Disiplin" value="Disiplin"/>
                        <Picker.Item label="Tanggung Jawab" value="Tanggung Jawab"/>
                        <Picker.Item label="Santun" value="Santun"/>
                        <Picker.Item label="Peduli" value="Peduli"/>
                        <Picker.Item label="Percaya Diri" value="Percaya Diri"/>
                  </Picker>
            ):(
                <Picker
                  selectedValue={butirsikap}
                  style={{height: 40, width: 300}}
                  onValueChange={(itemValue, itemIndex) =>
                    setButirsikap(itemValue)
                }>
                    <Picker.Item label="Ketaatan Beribadah" value="Ketaatan Beribadah"/>
                    <Picker.Item label="Berprilaku Syukur" value="Berprilaku Syukur"/>
                    <Picker.Item label="Berdoa sebelum dan sesudah melakukan kegiatan" value="Berdoa sebelum dan sesudah melakukan kegiatan"/>
                    <Picker.Item label="Toleransi dalam beragama" value="Toleransi dalam beragama"/>
                </Picker>
            )}  
          </Layout>
          <Layout style={{backgroundColor: '#f4f5f7', marginLeft:30, marginRight:30, marginBottom:5}}>
                    <Picker
                      selectedValue={penilaian}
                      style={{height: 40, width: 300}}
                      onValueChange={(itemValue, itemIndex) =>
                        setPenilaian(itemValue)
                    }>
                        <Picker.Item label="Sangat Baik" value="Sangat Baik"/>
                        <Picker.Item label="Baik" value="Baik"/>
                        <Picker.Item label="Perlu Bimbingan" value="Perlu Bimbingan"/>
                    </Picker>
          </Layout>
          <Layout style={{marginLeft:30, marginRight:30}}>
          <Layout>
            <Input 
              placeholder='Catatan Perilaku'
              multiline = {true}
              value={catatanperilaku}
              onChangeText={nextValue => setCatatanperilaku(nextValue)}
            /> 
          </Layout>
          <Layout>
            <Input 
              placeholder='Tindak Lanjut'
              multiline = {true}
              value={tindaklanjut}
              onChangeText={nextValue => setTindaklanjut(nextValue)}
            /> 
          </Layout>
          <Layout level='1'>
            <Button onPress={() => saveData()} status='info'  appearance='outline'>
              Simpan
            </Button>
          </Layout>
          </Layout>
      </Layout>
    )
}



const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    }
  });

  
  export default App;
