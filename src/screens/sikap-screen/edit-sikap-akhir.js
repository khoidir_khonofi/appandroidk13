import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { IndexPath, Layout, Input, Text, Button, Spinner } from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation}) {
  const [isLoad, setIsLoad] = useState(false)
  const [data, setData] = useState([]);
  const [isSave, setisSave] = useState(false)
  const [keterangansosial, setKeteranganSosial] = useState("");
  const [keteranganspiritual, setKeteranganSpiritual] = useState("");
  const indikator = (props) => {
    return isSave ? (
     <View style={[props.style, styles.indicator]}>
         <Spinner size='large'/>
     </View>                     
       ) : null
   }
  

  useEffect(() => {
    setIsLoad(true)
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/selectsikapakhir&id="+route.params.id).then((result) => {
      setKeteranganSosial(result.data.data.sosial)
      setKeteranganSpiritual(result.data.data.spiritual)
      setIsLoad(false)
    }).catch((e) => console.warn(e))
  },[])

  const saveData = () => {
    setisSave(true)
    axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/updatesikapakhir&id="+route.params.id, {
      id_sikap: route.params.id,
      keterangan_spiritual: keteranganspiritual,
      keterangan_sosial: keterangansosial,
    

    }).then((res) => {
        setisSave(false)
      alert(res.data);
  }).catch((e) => {alert('erros nyimpan'); setisSave(false)})
  }

  return (
      <>
      

<Layout style={styles.container} level='1'>
          
    <Layout style={{paddingRight: 30, paddingLeft: 30}}>
      
           {isLoad ? (
               <View style={[styles.container, styles.horizontal]}>
               <ActivityIndicator size="large" color="#0000ff" />
            </View>
          ) : null} 
        <Layout>
          <Input 
            placeholder='Keterangan Sosial'
            multiline = {true}
            value={keterangansosial}
            onChangeText={nextValue => setKeteranganSosial(nextValue)}
          /> 
        </Layout>

        <Layout>
          <Input 
            placeholder= 'Keterangan Spiritual'
            multiline = {true}
            value={keteranganspiritual}
            onChangeText={nextValue => setKeteranganSpiritual(nextValue)}
          /> 
        </Layout>

  </Layout>
        <Layout style={{ paddingRight: 30, paddingLeft: 30}} level='1'>
          <Button onPress={() => saveData()} status='info'  appearance='outline' accessoryLeft={indikator}>
            Simpan
            </Button>
      </Layout>
</Layout>
  




    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});