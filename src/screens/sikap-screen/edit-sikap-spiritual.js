import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View, AsyncStorage } from 'react-native';
import { IndexPath, Layout, Input, Text, Button, Spinner } from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation}) {
  const [isLoad, setIsLoad] = useState(false)
  const [data, setData] = useState([]);
  const [isSave, setisSave] = useState(false)
  
  
  const [nilai, setNilai] = useState("")
  const [nama, setNama] = useState("")
  const [butirSikap, setButirSikap] = useState("")

  const indikator = (props) => {
    return isSave ? (
     <View style={[props.style, styles.indicator]}>
         <Spinner size='large'/>
     </View>                     
       ) : null
   }

  useEffect(() => {
    setIsLoad(true)
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/butirsikapspiritual&id="+route.params.id).then((result) => {
        setNilai(result.data.data.nilai.toString())
        setButirSikap(result.data.data.butir_sikap)
        setNama(result.data.data.nama)
        setIsLoad(false)
    }).catch((e) => console.warn(e))
  },[])

  const saveData = () => {
    setisSave(true)
    axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/updatespiritual&id="+route.params.id, {
      id_sikap_spiritual: route.params.id,
      butir_sikap: butirSikap,
      nilai: nilai,
    }).then((res) => {
        setisSave(false)
        alert(res.data);
  }).catch((e) => {alert(e); setisSave(false)})
  }
 
  return (
      <>
        <Layout style={styles.container} level='1'>
       
           {isLoad ? (
               <View style={[styles.container, styles.horizontal]}>
               <ActivityIndicator size="large" color="#0000ff" />
            </View>
          ) : null}   

            <Layout style={{paddingRight: 30, paddingLeft: 30, marginBottom:10}}>
            <Text style={{marginBottom: 20, textAlign: 'center'}}>Edit sikap {nama}</Text>
                <Layout style={{backgroundColor: "#f4f5f7"}}>
                    <Picker
                    selectedValue={butirSikap}
                    style={{height: 50, width: 300}}
                    onValueChange={(itemValue, itemIndex) => {
                        setButirSikap(itemValue)}
                    }>
                    <Picker.Item label="Ketaatan Beribadah" value="Ketaatan Beribadah"/>
                    <Picker.Item label="Berprilaku Syukur" value="Berprilaku Syukur"/>
                    <Picker.Item label="Berdoa sebelum dan sesudah melakukan kegiatan" value="Berdoa sebelum dan sesudah melakukan kegiatan"/>
                    <Picker.Item label="Toleransi dalam beragama" value="Toleransi dalam beragama"/>
                    </Picker>
                </Layout>
            </Layout>
       



                <Layout style={{paddingRight: 30, paddingLeft: 30, marginBottom:10}}>
                <Input 
                    placeholder='Nilai'
                    value={nilai}
                    keyboardType = 'numeric'
                    onChangeText={nextValue => setNilai(nextValue)}
                /> 
                </Layout>

  
        <Layout style={{paddingRight: 30, paddingLeft: 30}} level='1'>
        <Button onPress={() => saveData()} status='info'  appearance='outline' accessoryLeft={indikator}>
            Simpan
            </Button>
        </Layout>
    </Layout>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});