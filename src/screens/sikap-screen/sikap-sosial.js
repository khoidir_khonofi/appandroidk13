import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { IndexPath, Layout, Input, Text, Button } from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation}) {
  const [dataSosial, setDataSosial] = useState([])
  const [isLoad, setIsLoad] = useState()
  const [sikapsosial, setSikapSosial] = useState("");
  const [isSave, setisSave] = useState(false)
  
  
  const [nilai, setNilai] = useState("")

  // useEffect(() => {
     
  //     axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/sikapsosial`).then((result) => {
        
    
    
  //         setDataSosial(result.data);
  //       }).catch((e)=> alert("Error Sikap sosial"))
        
  // }, [])

  const saveData = () => {
    setisSave(true)
    axios.post("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/sosial&id="+route.params.id, {
      id_siswa: route.params.id,
      butir_sikap: sikapsosial,
      nilai: nilai, 
    

    }).then((res) => {
setisSave(false)
      alert(JSON.stringify(res.data));
      navigation.navigate('Lihat Sikap Sosial')
  }).catch((e) => {alert("Eorror"); setisSave(false)})
  }

  return (
      <>
    <Layout style={styles.container} level='1'>
    
      <Layout style={{paddingRight: 30, paddingLeft: 30}}>
        <Text style={{fontSize: 20, textAlign: "center", marginBottom:10, borderBottomColor: "lightgrey"}}>Sikap Sosial</Text> 
        {isSave ? (
              <View style={[styles.container, styles.horizontal]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          ) : null}   
        
          <Layout style={{marginBottom: 5, borderWidth: 1, borderColor: 'grey', borderRadius: 5}}>
            <Picker
              selectedValue={sikapsosial}
              style={{height: 50, width: 300}}
              onValueChange={(itemValue, itemIndex) =>
                setSikapSosial(itemValue)
              }>
              <Picker.Item label="Jujur" value="Jujur"/>
                  <Picker.Item label="Disiplin" value="Disiplin"/>
                  <Picker.Item label="Tanggung Jawab" value="Tanggung Jawab"/>
                  <Picker.Item label="Santun" value="Peduli"/>
                  <Picker.Item label="Percaya Diri" value="Percaya Diri"/>
            </Picker>
          </Layout>
      
          <Layout style={{ marginTop: 10}}>
          <Input 
            placeholder='Nilai'
            value={nilai}
            keyboardType = 'numeric'
            onChangeText={nextValue => setNilai(nextValue)}
          /> 
          </Layout>
            
</Layout>
<Layout style={{marginTop: 20, paddingRight: 30, paddingLeft: 30}} level='1'>
    <Button onPress={() => saveData()} appearance='outline' status='info'>Simpan</Button>
</Layout>
</Layout>
  




    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});