import { Layout, Text, Avatar, MenuItem,
    Button, Card, Modal,Icon } from '@ui-kitten/components';
  import React, { useState, useEffect } from 'react';
  import axios from 'axios';
  import {View, ActivityIndicator, StyleSheet} from "react-native"
  import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
  import * as REDUXTYPES from './../../reduxs/type-redux/index'
  import { connect } from 'react-redux';
  
  
  const List =({data, nav, role})=>{
    const [visible, setVisible] = React.useState(false);
    const [idClick, setIdClick] = useState(0);

    const Delete = () => {
      axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/deletejurnal&id="+idClick).then((res) => {
          alert(res.data)
      }).catch(() => alert(e))
    }
  

  
    return(
      <Layout style={{marginHorizontal:5, backgroundColor:'transparent', borderRadius:5}} level='1'>
        <FlatList
          data={data}
          keyExtractor={(item, index) => {Math.random()}}
          renderItem={({item})=>{
            return(
              <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}} >
                
                  <Layout style={{flexDirection:'row'}} >
                        <Layout style={{flex:3, marginLeft:5, padding:3}}>
                          <Text>{item.nama_siswa} - {item.tingkat_kelas}{item.nama_kelas}</Text>
                          <Text>Sikap : {item.sikap}</Text>
                          <Text>Catatan Perilaku : {item.catatan_perilaku}</Text>
                          <Text>Butir Sikap : {item.butir_sikap}</Text>
                          <Text>Penilaian : {item.penilaian}</Text>
                          <Text>Tindak Lanjut : {item.tindak_lanjut}</Text>
                          <Text style={{color: 'grey', fontSize:12}}>{item.created_at}</Text>
                        </Layout>
                        {role === 'Administrator' || role === 'Guru' ? (
                            <Layout
                              style={{
                                flex: 1,
                                flexDirection: "row",
                                justifyContent: "flex-end",
                                alignItems: "center",
                              }}
                            >
                            <Icon name="more-vertical-outline" fill='green' onPress={() => {
                              setVisible(true);
                              setIdClick(item.id_jurnal_sikap)
                    
                            }} style={{  width: 32,
                              height: 32}}/>
                        </Layout>
                        ):null}
                        
                  </Layout>
               
              </Layout>
            )
          }}
        />
          <Modal 
                        style = {{width: 200, backgroundColor:"#1bcaff",}}
                        backdropStyle={styles.backdrop}
                        visible={visible} 
                        onBackdropPress={() => setVisible(false)}>
                           
                                   
                                    <MenuItem
                                      onPress={() =>{
                                        setVisible(false)
                                        Delete({ id: idClick })
                                        }
                                      }
                                      title="Delete"
                                    />

                                  
                      </Modal>
      </Layout>
    )
  }
  
  function App({navigation, auth}) {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
      username:null, role:null,
      email: null,
      token:null};
  
    const getApi=()=>{
      axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/jurnalsikap&token="+token).then((result) => {
       
        setData(result.data);
        setIsLoading(true)
      }).catch((e)=> alert(e))
      
    }
   
    useEffect(() => {
      getApi();
    },[]);
  
    
   
    return isLoading ? (
      <>
        <List nav={navigation} data={data} role={role}/>
      </>
    ) : (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )
  }
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
    modall: {
      width:200,
      backgroundColor:"#1bcaff",
      color :"red",
      borderRadius : 5,
    },
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.7)',
    }
  });
  
  const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(App);