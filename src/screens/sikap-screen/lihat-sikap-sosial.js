import React, { useState, useEffect } from 'react';
import { StyleSheet, Picker, ActivityIndicator, View, RefreshControl, SafeAreaView, ScrollView} from 'react-native';
import { Layout, Text , Avatar,MenuItem, Button, Card, Modal, Icon} from '@ui-kitten/components';
import axios from 'axios';
import { TouchableOpacity, FlatList} from 'react-native-gesture-handler';
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';


const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}
const List =({data, nav, role})=>{
  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useEffect(() => {
    setRefreshing(true);
    wait(1000).then(() => setRefreshing(false));
  }, []);

  const [idClick, setIdClick] = useState(0);
  const [idSiswa, setIdSiswa] = useState(0);
  const [name, setName] = useState("");
  const [status, setStatus] = useState("");
  const [visible, setVisible] = React.useState(false);
  const [isLoading, setIsLoading] = useState(false)

  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/delete&id="+idClick,{
    }).then((res) => {
      alert(res.data);
  }).catch((e) => {console.log('error hapus', e)})
  }
    return(
      <ScrollView contentContainerStyle={styles.scrollView}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }>
      <Layout style={{marginHorizontal:5, backgroundColor:'transparent', borderRadius:5}}>
        <FlatList
          data={data}
          keyExtractor={(item, index) => {item.id_sikap_sosial}}
          renderItem={({item})=>{
            return(
              <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}} level='1'>
                  <Layout style={{flexDirection:'row'}} level='1'>
                      
                      <Layout style={{flex:3, marginLeft:3, padding:5}} level='1'>
                        <Text>{item.nama_siswa} {item.tingkat_kelas}{item.nama_kelas}</Text>
                        <Text>Butir Sikap : {item.butir_sikap} </Text>
                        <Text>Nilai : {item.nilai}</Text>
                      </Layout>
                      <Layout
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                        level='1'
                      >
                        <Icon name="more-vertical-outline" fill='green' onPress={() => {
                          setVisible(true);
                          setIdClick(item.id_sikap_sosial)
                          setName(item.nama_siswa)
                          setIdSiswa(item.id_siswa)
                
                        }} style={{  width: 32,
                          height: 32}}/>
                       
                      </Layout>
                  </Layout>
               
              </Layout>
            
            )
          }}
        />
        <Modal 
                        style = {{width: 200, backgroundColor:"#1bcaff",}}
                        backdropStyle={styles.backdrop}
                        visible={visible} 
                        onBackdropPress={() => setVisible(false)}>
                           {role === 'Administrator' || role === 'Guru' ? (
                              <>
                                    <MenuItem onPress={() =>{ setVisible(false); nav.navigate("Edit Sikap Sosial", { id: idClick })
                                      }
                                      }
                                      title="Edit"
                                    />
                                     
                                    
                                    <MenuItem
                                      onPress={() =>{
                                        setVisible(false)
                                        Delete({ id: idClick })
                                        }
                                      }
                                      title="Delete"
                                    />

                                    <MenuItem
                                     onPress={() =>{ setVisible(false); nav.navigate('Lihat Sikap Akhir Siswa',{id_siswa: idSiswa})
                                    }
                                    }
                                      title={"Lihat Sikap Akhir "}
                                    />
                                     </>
                                    ):(
                                      <MenuItem
                                    onPress={() =>{ setVisible(false); nav.navigate('Lihat Sikap Akhir Siswa',{id_siswa: idSiswa})
                                    }
                                    }
                                      title={"Lihat Sikap Akhir "}
                                    />
                                    )}
                      </Modal>
      </Layout>
      </ScrollView>
    )
  }
  
  function App({navigation, auth}) {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
      username:null, role:null,
      email: null,
      token:null};
    const getApi=()=>{
      axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/sikapsosial&token="+token).then((result) => {
       
        setData(result.data);
        setIsLoading(true)

      }).catch((e)=> alert("Data kosong"))
      
    }
   
    useEffect(() => {
      getApi();
    },[]);
   
    return isLoading ? (
      <>
        <List nav={navigation} data={data} role={role}/>
      </>
    ) : (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )
  }
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
    backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  });

  const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(App);