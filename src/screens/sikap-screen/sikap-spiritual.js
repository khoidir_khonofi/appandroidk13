import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { IndexPath, Layout, Input, Text , Button} from '@ui-kitten/components';
import axios from "axios";

export default function App ({route}) {
  const [dataSpiritual, setDataSpiritual] = useState([])
  const [isLoad, setIsLoad] = useState(false)
  const [sikapspiritual, setSikapSpiritual] = React.useState(0);
  const [isSave, setisSave] = useState(false)
  
  
  const [nilai, setNilai] = useState("")



  const saveData = () => {
    setisSave(true)
    axios.post("http://192.168.43.13/tugasakhir/web/index.php?r=api/sikap/spiritual&id="+route.params.id, {
      id_siswa: route.params.id,
      butir_sikap: sikapspiritual,
      nilai: nilai, 
    

    }).then((res) => {
setisSave(false)
      alert(res.data);
  }).catch((e) => {alert("Eorror"); setisSave(false)})
  }

  return (
      <>
    <Layout style={styles.container} level='1'>
    
      <Layout style={{ paddingRight: 30, paddingLeft: 30}}>
        <Text style={{fontSize: 20, textAlign: "center", marginBottom:10, borderBottomColor: "lightgrey"}}>Sikap Spiritual</Text> 
        {isSave ? (
             <View style={[styles.container, styles.horizontal]}>
             <ActivityIndicator size="large" color="#0000ff" />
           </View>
          ) : null}   
       
          <Layout style={{marginBottom: 5, borderWidth: 1, borderColor: 'grey', borderRadius: 5}}>
       
          <Picker
              selectedValue={sikapspiritual}
              style={{height: 50, width: 300}}
              onValueChange={(itemValue, itemIndex) =>
                setSikapSpiritual(itemValue)
              }>
              <Picker.Item label="Ketaatan Beribadah" value="Ketaatan Beribadah"/>
                  <Picker.Item label="Berprilaku Syukur" value="Berprilaku Syukur"/>
                  <Picker.Item label="Berdoa sebelum dan sesudah melakukan kegiatan" value="Berdoa sebelum dan sesudah melakukan kegiatan"/>
                  <Picker.Item label="Toleransi dalam beragama" value="Toleransi dalam beragama"/>
            </Picker>
          </Layout>
     

          <Layout style={{marginBottom: 10}}>
          <Input 
            placeholder='Masuk Niliai'
            value={nilai}
            keyboardType = 'numeric'
            onChangeText={nextValue => setNilai(nextValue)}
          /> 
          </Layout>
            
</Layout>
<Layout style={{ paddingRight: 30, paddingLeft: 30}} level='1'>
<Button onPress={() => saveData()} appearance='outline' status='info'>Simpan</Button>
</Layout>
</Layout>
  




    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});