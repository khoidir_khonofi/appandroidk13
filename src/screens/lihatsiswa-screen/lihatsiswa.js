import {Layout, Text, Avatar, Icon} from '@ui-kitten/components';
import React, {useState, useEffect, useRef} from 'react';
import axios from 'axios';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import {TouchableOpacity, FlatList} from 'react-native-gesture-handler';
import RBSheet from 'react-native-raw-bottom-sheet';
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';

// const dataOptions = ['Nilai Keterampilan', 'Nilai Pengetahuan'];

function App({route, navigation, auth}) {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const refRBSheet = useRef();
  const [idSiswa, setIdSiswa] = useState(0);
  let {token, email, role} = auth.login?JSON.parse(auth.login): {
    username:null, role:null,
    email: null,
    token:null};
  const [jenjangpendidikan, setstate] = useState("")
  useEffect(() => {
    axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/user/sekolah&token=${token}`)
    .then((res) => setstate(res.data)).catch((e) => alert(e))
  }, [])

  const getApi = async () => {
    let result = await axios(
      'http://192.168.43.13/tugasakhir/web/index.php?r=api/mengajar/kelas&id=' +
        route.params.id_kelas,
    );

    if (result) {
      setIsLoading(true);

      setData(result.data);
    }
  };

  useEffect(() => {
    (async function run() {
      await getApi();
    })();
  }, []);

  return isLoading ? (
    <Layout style={{marginHorizontal: 5, backgroundColor: 'transparent'}}>
      <FlatList
        data={data}
        keyExtractor={(item, index) => {item.id_siswa}}
        renderItem={({item}) => {
          return (
            <Layout
              style={{marginTop: 5, backgroundColor: '#1bcaff', borderRadius: 15}}>
              <Layout>
                <Layout style={{flexDirection: 'row'}}>
                  <Layout style={{padding: 5}}>
                    <Avatar
                      resizeMode={'cover'}
                      source={{uri: item.siswa.foto}}
                    />
                  </Layout>
                  <Layout
                    style={{
                      flex: 1,
                      marginLeft: 16,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text>
                      {item.siswa.nama}({item.siswa.nis})
                    </Text>
                    <Text></Text>
                  </Layout>
                  {role === 'Administrator' || role === 'Guru' ? (
                       <Layout
                       style={{
                         flex: 1,
                         flexDirection: 'row',
                         justifyContent: 'flex-end',
                         alignItems: 'center',
                         paddingRight: 2,
                       }}>
                       <>
                         <TouchableOpacity
                           onPress={() => {
                             refRBSheet.current.open();
                             setIdSiswa(item.siswa.id);
                           }}>
                           <Icon
                             style={{width: 32, height: 32}}
                             fill="#8F9BB3"
                             name="more-vertical-outline"
                           />
                         </TouchableOpacity>
                       </>
                     </Layout>
                  ):null}
                 
                </Layout>
              </Layout>
            </Layout>
          );
        }}
      />
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        animationType = "fade"
        height ={300}
        customStyles={{
          wrapper: {
            backgroundColor: 'rgba(0,0,0,0.5)',
          },
          draggableIcon: {
            backgroundColor: 'black',
          },
        }}>
        <Layout>
          {jenjangpendidikan === 'SMA' ? (
            <>
               <Text
               onPress={() => {refRBSheet.current.close(); navigation.navigate('Sma Harian Keterampilan',{id: idSiswa})}}
                 style={{
                  color : 'black',
                  padding: 10,
                  fontSize: 18,
                  textAlign: 'center',
                  height: 50,
                  borderBottomWidth: 1,
                  borderColor: 'grey',
                 }}>
                Input Nilai Keterampilan
               </Text>
             <Text
               onPress={() => {refRBSheet.current.close(); navigation.navigate('Sma Harian Pengetahuan',{id: idSiswa})}}
                 style={{
                  color : 'black',
                  padding: 10,
                  fontSize: 18,
                  textAlign: 'center',
                  height: 50,
                  borderBottomWidth: 1,
                  borderColor: 'grey',
                 }}>
                 Input Nilai Pengetahuan
               </Text>
               </>
          ): jenjangpendidikan === 'SMP' ? (
              <>
            <Text
            onPress={() => {refRBSheet.current.close(); navigation.navigate('Smp Harian Pengetahuan',{id: idSiswa})}}
              style={{
                    color : 'black',
                    padding: 10,
                    fontSize: 18,
                    textAlign: 'center',
                    height: 50,
                    borderBottomWidth: 1,
                    borderColor: 'grey',
              }}>
              Input Nilai Pengetahuan
            </Text>
            <Text
            onPress={() => {refRBSheet.current.close(); navigation.navigate('Smp Harian Keterampilan',{id: idSiswa})}}
              style={{
                color : 'black',
                padding: 10,
                fontSize: 18,
                textAlign: 'center',
                height: 50,
                borderBottomWidth: 1,
                borderColor: 'grey',
              }}>
              Input Nilai Keterampilan
            </Text>
            </>
          ): jenjangpendidikan === 'SD' ? (
            <>
                <Text
                onPress={() => {refRBSheet.current.close(); navigation.navigate('Sd Harian Pengetahuan',{id: idSiswa})}}
                  style={{
                    color : 'black',
                    padding: 10,
                    fontSize: 18,
                    textAlign: 'center',
                    height: 50,
                    borderBottomWidth: 1,
                    borderColor: 'grey',
                  }}>
                    Input Nilai Pengetahuan
                </Text>
                 <Text
                 onPress={() => {refRBSheet.current.close(); navigation.navigate('Sd Harian Keterampilan',{id: idSiswa})}}
                   style={{
                     color : 'black',
                     padding: 10,
                     fontSize: 18,
                     textAlign: 'center',
                     height: 50,
                     borderBottomWidth: 1,
                     borderColor: 'grey',
                   }}>
                   Input Nilai Keterampilan
                 </Text>
                 </>
          ): null}
        
      
      
          <Text
          onPress={() => {refRBSheet.current.close(); navigation.navigate('Sikap Sosial',{id: idSiswa})}}
            style={{
              color : 'black',
              padding: 10,
              fontSize: 18,
              textAlign: 'center',
              height: 50,
              borderBottomWidth: 1,
              borderColor: 'grey',
            }}>
            Sikap Sosial
          </Text>
          <Text
          onPress={() => {refRBSheet.current.close(); navigation.navigate('Sikap Spiritual',{id: idSiswa})}}
            style={{
              padding: 10,
              color : 'black',
              fontSize: 18,
              textAlign: 'center',
              height: 50,
              borderBottomWidth: 1,
              borderColor: 'grey',
            }}>
            Sikap Spiritual
          </Text>
          <Text
          onPress={() => {refRBSheet.current.close(); navigation.navigate('Input Jurnal Sikap',{id: idSiswa})}}
            style={{
              padding: 10,
              color : 'black',
              fontSize: 18,
              textAlign: 'center',
              height: 50,
              borderBottomWidth: 1,
              borderColor: 'grey',
            }}>
            Input Jurnal Sikap
          </Text>
        </Layout>
      </RBSheet>
    </Layout>
  ) : (
    <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="large" color="#0000ff" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
});

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
