import {Layout, Text, Avatar, Icon} from '@ui-kitten/components';
import React, {useState, useEffect, useRef} from 'react';
import axios from 'axios';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import {TouchableOpacity, FlatList} from 'react-native-gesture-handler';
import RBSheet from 'react-native-raw-bottom-sheet';
import * as REDUXTYPES from '../../reduxs/type-redux/index'
import { connect } from 'react-redux';

function App({route, auth}){
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
        username:null, role:null,
        email: null,
        token:null};
    const [data, setData] = useState("")
    const [isloading, setIsloading] = useState(false)

    useEffect(() => {
        setIsloading(true)
        axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/siswa&token=${token}`)
            .then((res) => {
               
                setData(res.data)
                setIsloading(false)
            })
                .catch((e) => alert(e))
    }, [])


    return !isloading ? (
        <Layout style={{marginHorizontal: 5, backgroundColor: 'transparent'}}>
            <FlatList
                data={data}
                keyExtractor = {(index) => {index}}
                renderItem = {({item}) => {
                    return (
                        <Layout style={{marginTop: 5, backgroundColor: '#1bcaff'}}>
                            <Layout style={{flexDirection: 'row'}}>
                                <Layout style={{padding: 15}} level='1'>
                                    <Avatar
                                        style={{height:50, width: 50}}
                                        resizeMode={'cover'}
                                        source={{uri: item.foto}}
                                    />
                                </Layout>
                                <Layout style={{flex: 3, marginLeft: 3}} level='2'>
                                    <Text>Nama : {item.nama} - {item.kelas}{item.nama_kelas}</Text>
                                    <Text>Nis : {item.nis}</Text>
                                    <Text>Tempat Lahir : {item.tempat_lahir}</Text>
                                    <Text>Tanggal Lahir : {item.tanggal_lahir}</Text>
                                    <Text>Alamat : {item.alamat}</Text>
                                </Layout>
                            </Layout>
                        </Layout>
                    )
                }}
            />
        </Layout>
    ):(
        <Text>memuat...</Text>
    )


}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
  });
  
  const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(App);