import React, { useState, useEffect } from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { Layout, Text , Avatar, MenuItem,
  Button, Card, Modal,Icon} from '@ui-kitten/components';
import axios from 'axios';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';

const List =({data, nav, role})=>{
  const [visible, setVisible] = React.useState(false);
  const [idClick, setIdClick] = useState(0);
  const [idSiswa, setIdSiswa] = useState(0);
  const [nama, setNama] = useState(0);
  

  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilai/deletenilaikdpengetahuan&id="+idClick,{
    }).then((res) => {
      alert(res.data);
  }).catch((e) => {console.log('error hapus', e)})
  }

    return(
      <Layout style={{marginHorizontal:5, backgroundColor:'transparent', borderRadius:5}}>
        <FlatList
          data={data}
          keyExtractor={(item, index) => {item.nilai.id}}
          renderItem={({item})=>{
            return(
              <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}}>
                {/* <TouchableOpacity onPress={() => nav.navigate('Lihat Nilai Akhir Detail',{id_siswa:item.id_siswa})}> */}
                  <Layout style={{flexDirection:'row'}}>
                      
                      <Layout style={{flex:3, marginLeft:3, padding:5}}>
                        <Text>{item.siswa.nama}</Text>
                        <Text>Mata Pelajaran : {item.mapel.nama_mata_pelajaran}</Text>
                        <Text>KD : {item.kdpengetahuan.no_kd}</Text>
                        <Text>KKM : {item.kkm.kkm}</Text>
                        <Text>Nph : {item.nilai.nph} {item.nilai.nph > item.kkm.kkm ? (
                                  <Text>- telampaui</Text>
                              ): item.nilai.nph === item.kkm.kkm ? (
                                  <Text>- Tercapai</Text>
                              ):(
                                  <Text>- Tidak Tercapai</Text>
                              )}</Text>
                        <Text>Npts : {item.nilai.npts}</Text>
                        <Text>Npas : {item.nilai.npas}</Text>
                      </Layout>
                      <Layout
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                        >
                        <Icon name="more-vertical-outline" fill='#8F9BB3' onPress={() => {
                          setVisible(true);
                          setIdClick(item.nilai.id);
                          setIdSiswa(item.siswa.id_siswa)
                          setNama(item.siswa.nama)
                          
                          
                        }} style={{  width: 32,
                          height: 32}}/>
                      </Layout>
                  </Layout>
              </Layout>
            )
          }}
        />

                <Modal 
                  visible={visible} 
                  backdropStyle={styles.backdrop}
                  style={styles.modall}
                  onBackdropPress={() => setVisible(false)}>
                      {role === 'Administrator' || role === 'Guru' ? (
                        <>
                        <MenuItem
                                onPress={() =>{
                                  setVisible(false)
                                  nav.navigate("Input Nilai Ujian", { id: idClick })
                                  
                                }
                                }
                                
                                title="Npts dan Npas"
                              />

                              <MenuItem
                                onPress={() =>{
                                  setVisible(false)
                                  nav.navigate("Lihat Nilai Akhir Detail", { id: idSiswa })
                                  
                                }
                                }
                                
                                title={"Lihat Nilai Akhir "+nama}
                              />
                              
                              <MenuItem
                                onPress={() =>{

                                  setVisible(false)
                                  Delete({ id: idClick })
                                  }
                                }
                                
                                title="Delete"
                              />
                        </>
                      ):(
                        <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Lihat Nilai Akhir Detail", { id: idSiswa })
                          
                        }
                        }
                        
                        title={"Lihat Nilai Akhir "+nama}
                      />
                      )}
                              
                </Modal>
      </Layout>
    )
  }
  
  function App({navigation, route, auth}) {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
      username:null, role:null,
      email: null,
      token:null};
    const getApi=()=>{
        axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaikd/nilaikdpengetahuan&id="+route.params.id_siswa).then((result) => {
         
          setData(result.data);
          setIsLoading(true)
        }).catch((e)=> alert(e))
        
      }
   
    useEffect(() => {
      getApi();
    },[]);
   
    return isLoading ? (
      <>
        <List nav={navigation} data={data} role={role}/>
      </>
    ) : (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )
  }
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modall: {
      width:200,
      backgroundColor:"#1bcaff",
      color :"red",
      borderRadius : 5,
    }
  });

  const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(App);