import React, { useState, useEffect } from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { Layout, Text , Avatarr, MenuItem,
  Button, Card, Modal,Icon} from '@ui-kitten/components';
import axios from 'axios';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';

const List =({data, nav, role})=>{
  const [visible, setVisible] = React.useState(false);
  const [idClick, setIdClick] = useState(0);
  const [idSiswa, setIdSiswa] = useState(0);
  const [nama, setNama] = useState(0);

  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilai/deletepengetahuan&id="+idClick).then((res) => {
      alert(res.data);
  }).catch((e) => {console.log('error hapus', e)})
  }
    return(
      <Layout style={{marginHorizontal:5, backgroundColor:'transparent', borderRadius:5}}>
        <FlatList
          data={data}
          keyExtractor={(item, index) => {item.id_nilai_harian_pengetahuan}}
          renderItem={({item})=>{
            return(
              <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}} id={item.id_nilai_harian_pengetahuan} level='1'>
                  <Layout style={{flexDirection:'row'}} level='1'>
                      
                      <Layout style={{flex:2.5, marginLeft:3, padding:5}} level='1'>
                        <Text>{item.nama_siswa} {item.tingkat_kelas}{item.nama_kelas}</Text>
                        <Text>Mata Pelajaran : {item.nama_mata_pelajaran}</Text>
                        <Text>KD : {item.no_kd} </Text>
                        <Text>Tema : {item.tema}</Text>
                        <Text>Subtema : {item.subtema}</Text>
                        <Text>Nama Penilaian : {item.nama_penilaian}</Text>
                        <Text>Nilai : {item.nilai}</Text>
                        <Text style={{fontSize:12}}>Waktu : {item.created_at}</Text>
                      </Layout>
                      <Layout
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                        level='1'
                        >
                        <Icon name="more-vertical-outline" fill='#8F9BB3' onPress={() => {
                          setVisible(true);
                          setIdClick(item.id_nilai_harian_pengetahuan);
                          setIdSiswa(item.id_siswa)
                          
                          
                        }} style={{  width: 32,
                          height: 32}}/>
                      </Layout>
                  </Layout>
              </Layout>
            )
          }}
        />
        <Modal 
          visible={visible} 
          backdropStyle={styles.backdrop}
          style={styles.modall}
          onBackdropPress={() => setVisible(false)}>
                {role === 'Administrator' || role === 'Guru' ? (
                  <>
                     <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Lihat Nilai Kd Pengetahuan", { id_siswa: idSiswa })
                          
                        }
                        }
                        
                        title="Lihat Nilai KD"
                      />
                      <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Edit Nilai Harian Pengetahuan SD", { id: idClick })
                          
                        }
                        }
                        
                        title="Edit"
                      />

                      <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          Delete({ id: idClick })
                          }
                        }
                        
                        title="Delete"
                      />
                  </>
                ):(
                  <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Lihat Nilai Kd Pengetahuan", { id_siswa: idSiswa })
                          
                        }
                        }
                        
                        title="Lihat Nilai KD"
                      />
                )}
                     
        </Modal>
      </Layout>
    )
  }
  
  function App({navigation, auth}) {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
      username:null, role:null,
      email: null,
      token:null};
    const getApi=()=>{
      axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilai/indexpengetahuan&token="+token).then((result) => {
       
        setData(result.data);
        setIsLoading(true)
      }).catch((e)=> alert(e))
      
    }
   
    useEffect(() => {
      getApi();
    },[]);
   
    return isLoading ? (
      <>
        <List nav={navigation} data={data} role={role}/>
      </>
    ) : (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )

  
  }
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.7)',
    },
    modall: {
      width:200,
      backgroundColor:"#1bcaff",
      color :"red",
      borderRadius : 5,
    }
  });

  const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(App);