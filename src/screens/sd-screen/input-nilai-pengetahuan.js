import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { IndexPath, Layout, Input, Text, Button, Spinner } from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation}) {
  const [dataMapel, setDataMapel] = useState([])
  const [dataKd, setDataKd] = useState([])
  const [dataTema, setDataTema] = useState([])
  const [state, setstate] = useState([])
  const [Subtema, setSubtema] = useState([])
  const [isLoad, setIsLoad] = useState(false)
  const [selectedValue, setSelectedValue] = React.useState(0);
  const [getValKd, setGetValKd] = useState(0)
  const [getTema, setGetTema] = useState(0)
  const [getIdSubTema, setgetIdSubTema] = useState(0)
  const [isSave, setisSave] = useState(false)
  const [namapenilaian, setNamapenilaian] = useState("")
  const [nilai, setNilai] = useState("")
  const [x, setX] = useState("")


  const indikator = (props) => {
   return isSave ? (
    <View style={[props.style, styles.indicator]}>
        <Spinner size='large'/>
    </View>                     
      ) : null
  }

  useEffect(() => {
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilai/mapel&id="+route.params.id).then((result) => {
    
    setIsLoad(true);
      
      setDataMapel(result.data.datamapel);
      setstate(result.data.Tingkatkelas)
    }).catch((e)=> console.log(e))


    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilai/kade").then((result) => {
        setDataKd(result.data);
      }).catch((e)=> alert("Error KD"))

    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilai/tema").then((result) => {
        setDataTema(result.data);
      }).catch((e)=> alert("Error Tema"))
  
     
    axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/nilai/subtema`).then((result) => {
        setSubtema(result.data);
      }).catch((e)=> alert("Error subTema"))
        
  }, [])

  const saveData = () => {
    setisSave(true)
    axios.post("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilai/sdinputnilaipengetahuan", {
      id_siswa: route.params.id,
      id_kd_pengetahuan: getValKd,
      id_tema: getTema,
      id_subtema: getIdSubTema,
      id_mapel: selectedValue,
      nama_penilaian : namapenilaian,
      nilai: nilai, 
    

    }).then((res) => {
setisSave(false)
      alert(res.data);
  }).catch((e) => {console.log(e); setisSave(false)})
  }

  return (
      <>
    <Layout style={styles.container} level='1'>
    
      <Layout style={{paddingRight: 30, paddingLeft: 30}}>
      <Text style={{fontSize: 20, textAlign: "center", marginBottom:10, borderBottomColor: "lightgrey"}}>Input Nilai Pengetahuan</Text> 
       
        {/* Mapel */}
          <Layout style={{marginBottom: 5, borderWidth: 1, borderColor: 'grey', borderRadius: 5}}>
       
            <Picker
              selectedValue={selectedValue}
              onValueChange={(value, index) => setSelectedValue(value)}
              >
              {dataMapel.map((val, key) =>  {
                return (
                  <Picker.Item label={val.nama_mata_pelajaran} value={val.id_mapel}/>
                      
                )
              }) }
            </Picker>
          </Layout>

            {/* KD */}
          <Layout style={{marginBottom: 5, borderWidth: 1, borderColor: 'grey', borderRadius: 5}}>
            <Picker
              selectedValue={getValKd}
              onValueChange={(value, index) => setGetValKd(value)}
            > 
              {dataKd.length > 0 ? (
                dataKd.filter((e) => {
                  if(e.id_mapel==selectedValue){
                    return e
                  }
                  
                }).map((val, key) => {
                  return (

                    <Picker.Item label={val.no_kd} value={val.id_kd_pengetahuan}/>
                        
                  )
                }) 

              ) : (
                <Picker.Item label="Data tidak tersedia"/>
              )}
            
            </Picker>
          </Layout>


          <Layout style={{marginBottom: 5, borderWidth: 1, borderColor: 'grey', borderRadius: 5}}>
            
            <Picker
                selectedValue={getTema}
                onValueChange={(value, index) => setGetTema(value)}
              > 
                {dataTema.length > 0 ? (
                dataTema.filter((e) => {
                  if(e.id_kd==getValKd){
                    return e
                  }
                  
                }).map((val, key) => {
                  return (

                    <Picker.Item label={val.tema} value={val.id_tema}/>
                        
                  )
                }) 

              ) : (
                <Picker.Item label="Data tidak tersedia"/>
              )}
            
              
            </Picker>
          </Layout>


          {/* sUBTEma */}
          <Layout style={{marginBottom: 5, borderWidth: 1, borderColor: 'grey', borderRadius: 5}}>
              <Picker
              selectedValue={getIdSubTema}
              onValueChange={(value, index) => setgetIdSubTema(value)}
            > 
            {Subtema.length > 0 ? (
              Subtema.filter((e) => {
                if(e.id_tema==getTema){
                  return e
                }
                
              }).map((val, key) => {
                return (

                  <Picker.Item label={val.subtema} value={val.id_subtema}/>
                      
                )
              }) 

            ) : (
              <Picker.Item label="Data tidak tersedia"/>
            )}
            
              </Picker>
          </Layout>
          <Layout>
          <Input 
            placeholder='Nama Penilaian, ex: Latihan1'
            value={namapenilaian}
            onChangeText={nextValue => setNamapenilaian(nextValue)}
          /> 
          </Layout>

          <Layout>
          <Input 
            placeholder='Nilai'
            value={nilai}
            keyboardType = 'numeric'
            onChangeText={nextValue => setNilai(nextValue)}
          /> 
          </Layout>
            
</Layout>
<Layout style={{paddingRight: 30, paddingLeft: 30}} level='1'>
    <Button onPress={() => saveData()} appearance='outline' status='info' accessoryLeft={indikator}>Simpan</Button>
</Layout>
</Layout>
  




    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});