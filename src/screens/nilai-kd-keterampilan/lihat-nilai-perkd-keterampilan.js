import React, { useState, useEffect } from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { Layout, Text , Avatar, MenuItem,
  Button, Card, Modal,Icon} from '@ui-kitten/components';
import axios from 'axios';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';

const List =({data, nav,  role})=>{
  const [visible, setVisible] = React.useState(false);
  const [idClick, setIdClick] = useState(0);
  const [idSiswa, setIdsiswa] = useState(0)
  const [nama, setNama] = useState("")

  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaisma/deletepengetahuan&id="+idClick).then((respon) => {
        alert(respon.data);
    }).catch((e) => console.log(e))
  }

    return(
      <Layout style={{marginHorizontal:5, backgroundColor:'transparent', borderRadius:5}}>
        <FlatList
          data={data}
          keyExtractor={(item, index) => item.id_nilai_kd_keterampilan}
          renderItem={({item})=>{
            return(
              <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}}>
                  <Layout style={{flexDirection:'row'}}>
                      
                      <Layout style={{flex:3, marginLeft:3, padding:5}}>
                        <Text>Nama : {item.siswa.nama}</Text>
                        <Text>Mata Pelajaran : {item.mapel.nama_mata_pelajaran}</Text>
                        <Text>KD : {item.kdketerampilan.no_kd}</Text>
                        <Text>Skor : {item.nilai.skor}</Text>
                      </Layout>
                      <Layout
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                        >
                        <Icon name="more-vertical-outline" fill='#8F9BB3' onPress={() => {
                          setVisible(true);
                          setIdClick(item.nilai.id);
                          setIdsiswa(item.siswa.id_siswa)
                          setNama(item.siswa.nama)
                          
                          
                        }} style={{  width: 32,
                          height: 32}}/>
                      </Layout>
                  </Layout>
              </Layout>
            )
          }}
        />
         <Modal 
          visible={visible} 
          backdropStyle={styles.backdrop}
          style={styles.modall}
          onBackdropPress={() => setVisible(false)}>
            {role === 'Administrator' || role === 'Guru' ? (
              <>
                 <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Lihat Nilai Akhir Detail", { id: idSiswa })
                          
                        }
                        }
                        
                        title={"Lihat Nilai Akhir Detail"}
                      />

                      <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          Delete({ id: idClick })
                          }
                        }
                        
                        title={"Delete "+nama}
                      />
              </>
            ):(
              <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Lihat Nilai Akhir Detail", { id: idSiswa })
                          
                        }
                        }
                        
                        title={"Lihat Nilai Akhir Detail"}
                      />
            )}
                     
        </Modal>
      </Layout>
    )
  }
  
  function App({navigation, route, auth}) {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
      username:null, role:null,
      email: null,
      token:null};
    const getApi=()=>{
        axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaikd/nilaiperkdketerampilan&id="+route.params.id_siswa).then((result) => {
         
          setData(result.data);
          setIsLoading(true)
        }).catch((e)=> alert("Error"))
        
      }
   
    useEffect(() => {
      getApi();
    },[]);
   
    return isLoading ? (
      <>
        <List nav={navigation} data={data} role={role}/>
      </>
    ) : (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )
  }
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
    modall: {
      width:200,
      backgroundColor:"#1bcaff",
      color :"red",
      borderRadius : 5,
    },
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.7)',
    }
  });

  const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(App);