import React, {useState, useEffect} from 'react';
import { StyleSheet, AsyncStorage, ActivityIndicator, View } from 'react-native';
import { Layout, Text, Button, Icon } from '@ui-kitten/components';
import { ScrollView } from 'react-native-gesture-handler';
import Axios from "axios"
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';


function Lihatnilai ({navigation, auth}){
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
        username:null, role:null,
        email: null,
        token:null};
    const [jenjangpendidikan, setstate] = useState("")
    
    useEffect(() => {
      Axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/user/sekolah&token=${token}`)
      .then((res) => setstate(res.data)).catch((e) => alert('er'))
    }, [])
    
  return (
    <>
    <ScrollView>
       
        { jenjangpendidikan == 'SMA'  ?  (
                <>
                <Layout style={{marginTop: 30, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Sma Lihat Harian Pengetahuan')}   appearance='outline' status='info'>SMA Nilai Pengetahuan</Button>
                </Layout>

                <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Sma Lihat Harian Keterampilan')}   appearance='outline' status='info'>SMA Nilai Keterampilan</Button>
                </Layout>

                <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Lihat Semua KD')}   appearance='outline' status='info'>Lihat Semua KD Pengetahuan</Button>
                </Layout>

                <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Lihat Total KD')}   appearance='outline' status='info'>Lihat Total KD Pengetahuan</Button>
                </Layout>
                </>
            ): jenjangpendidikan == 'SMP' ? (
                <>
                 <Layout style={{marginTop: 30, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Smp Lihat Harian Pengetahuan')}   appearance='outline' status='info'>SMP Nilai Pengetahuan</Button>
                </Layout>
               
                <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Smp Lihat Harian Keterampilan')}   appearance='outline' status='info'>SMP Nilai Keterampilan</Button>
                </Layout>

                <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Lihat Semua KD')}   appearance='outline' status='info'>Lihat Semua KD Pengetahuan</Button>
                </Layout>
               
                <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Lihat Total KD')}   appearance='outline' status='info'>Lihat Total KD Pengetahuan</Button>
                </Layout>
                </>
            ): jenjangpendidikan == 'SD' ? (
                <>
                <Layout style={{marginTop: 30, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Sd Lihat Harian Pengetahuan')}   appearance='outline' status='info'> SD Nilai Pengetahuan</Button>
                </Layout>

                <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Sd Lihat Harian Keterampilan')}   appearance='outline' status='info'>SD Nilai Keterampilan</Button>
                </Layout>

                <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
                    <Button  onPress={() => navigation.navigate('Lihat Nilai Semua KD')}   appearance='outline' status='info'> Lihat Nilai KD Pengetahuan</Button>
                </Layout>
               
                </>

            ): null
        }
      
     
      
        <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
          <Button  onPress={() => navigation.navigate('Lihat Nilai Akhir')}   appearance='outline' status='info'>
          Lihat Nilai Akhir</Button>
        </Layout>

        <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
            <Button  onPress={() => navigation.navigate('Nilai Akhir KKM Satuan Pendidikan')}   appearance='outline' status='info'>Lihat Nilai Akhir KKM Satuan Pendidikan </Button>
        </Layout>
      
        <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
            <Button  onPress={() => navigation.navigate('Lihat Sikap Sosial')}   appearance='outline' status='info'>
            Sikap Sosial</Button>
        </Layout>

        <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
            <Button  onPress={() => navigation.navigate('Lihat Sikap Spiritual')}   appearance='outline' status='info'> Sikap Spiritual</Button>
        </Layout>

        <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30}} level='1'>
            <Button  onPress={() => navigation.navigate('Lihat Sikap Akhir')}   appearance='outline' status='info'> Sikap Akhir</Button>
        </Layout>

        <Layout style={{marginTop: 10, marginRight: 30, marginLeft: 30, marginBottom:10}} level='1'>
            <Button  onPress={() => navigation.navigate('Lihat Jurnal Sikap')}   appearance='outline' status='info'> Lihat Jurnal Sikap</Button>
        </Layout>

        {role === 'Administrator'  || role === 'Guru' ? (
            <>
                <Layout style={{marginRight: 30, marginLeft: 30, marginBottom:20}} level='1'>
                    <Button  onPress={() => navigation.navigate('Data Siswa')}   appearance='outline' status='info'> Lihat Siswa</Button>
                </Layout>
            </>
        ):null}
      </ScrollView>
    </>
  )
}

// conect to redux
const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(Lihatnilai);

// export default Lihatnilai;