import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator,  View, AsyncStorage,TextInput } from 'react-native';
import { IndexPath, Layout, Button, Input, Text, Icon, Spinner } from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation }) {
    const [isSave, setisSave] = useState(false)
    const [isload, setIsload] = useState(false)
    const [email, setEmail] = useState("")
    const [role, setRole] = useState("")
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [npsn, setNpsn] = useState("")
    const [datasekolah, setSekolah] = useState([])
    const [datawalikelas, setDatawalikelas] = useState([])
    const [datakelas, setDatakelas] = useState([])
    const [jenjangpendidikan, setJenjangpendidikan] = useState("")
    const [isopen, setIsopen] = useState(false)
    const [idSekolah, setIdsekolah] = useState(0)
    const [idGuru, setIdguru] = useState("")
    const [testing, setTesting] = useState("")
    const [kelasidguru, setKelasidguru] = useState(0)
    const [idKelas, setIdkelas] = useState(0)
    const [nama, setNama] = useState("")
    const StarIcon = (props) => (
      <Icon {...props} name='log-in-outline'/>
    );

    const indikator = (props) => {
        return isSave ? (
         <View style={[props.style, styles.indicator]}>
             <Spinner size='large'/>
         </View>                     
           ) : null
       }

       useEffect(() => {
        setIsload(true)
        axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/site/pilihsekolah").then((result) => {
          
          setSekolah(result.data)
          setIsload(false)
        }).catch((e) => alert(e), setIsload(false))
      
      
        axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/site/pilihkelas").then((result) => {
          
         setDatakelas(result.data)
          setIsload(false)
        }).catch((e) => alert("erros kelas"), setIsload(false))
      
      
          }, [])

          const Register = () => {
          
            setisSave(true)
            axios.post("http://192.168.43.13/tugasakhir/web/index.php?r=api/site/register", {
              email : email,
              username: username,
              password: password,
              jenjang_pendidikan : jenjangpendidikan,
              npsn: npsn,
              role : role,
              id_sekolah : idSekolah,
              id_kelas : idKelas,
            }).then((res) => {
              setisSave(false)

              alert(res.data);
          }).catch((e) => {alert(e); setisSave(false)})
          }

    return (
        <>
        <Layout style={{marginLeft: 30, marginRight: 30, flex:1, justifyContent: 'center', backgroundColor : 'transparent'}}>
        <Text style={{textAlign: 'center', marginBottom:20, fontSize:20}} status='info'>Daftar</Text>
         <Layout>
         <Input 
                placeholder='Email'
                value={email}
                onChangeText={nextValue => setEmail(nextValue)}
              /> 
          </Layout>

          <Layout>
          <Input 
                placeholder='Username'
                value={username}
                onChangeText={nextValue => setUsername(nextValue)}
              /> 
          </Layout>

          <Layout style={{marginBottom:5}}>
          <Input 
                placeholder='Password'
                value={password}
                onChangeText={nextValue => setPassword(nextValue)}
              /> 
          </Layout>
          <Layout style={{backgroundColor: '#f4f5f7',marginBottom: 10}} id='status'>
              <Picker
                selectedValue={role}
                style={{height: 40, width: 300}}
                onValueChange={(itemValue, itemIndex) =>
                  setRole(itemValue)
                }>
                   <Picker.Item label="Sekolah" value="Administrator"/>
                  <Picker.Item label="Guru" value="Guru"/>
                    <Picker.Item label="Siswa" value="Siswa"/>
                   
              </Picker>
            </Layout>
            {role === 'Administrator' ? (
              <>
            <Layout style={{backgroundColor: '#f4f5f7',marginBottom: 10}}>
              <Picker
                selectedValue={jenjangpendidikan}
                style={{height: 40, width: 300}}
                onValueChange={(itemValue, itemIndex) =>
                  setJenjangpendidikan(itemValue)
                }>
                  <Picker.Item label="SD" value="SD"/>
                    <Picker.Item label="SMP" value="SMP"/>
                    <Picker.Item label="SMA" value="SMA"/>
              </Picker>
            </Layout>

              <Layout style={{marginBottom: 20}}>
                <Input 
                  placeholder='Npsn'
                  value={npsn}
                  onChangeText={nextValue => setNpsn(nextValue)}
                /> 
              </Layout> 
            </>
            ): role === 'Guru' ? (
              <Layout style={{backgroundColor: '#f4f5f7',marginBottom: 10}}>
              <Picker
                selectedValue={idSekolah}
                style={{height: 40, width: 300}}
                onValueChange={(itemValue, itemIndex) => setIdsekolah(itemValue)}
                  >
                  {datasekolah.map((val) =>  {
                    return (
                      <Picker.Item label={val.nama_sekolah} value={val.id_sekolah}/>
                      
                    )
                  }) }
                   
              </Picker>
            </Layout>
            ): (
              <>
              <Layout style={{backgroundColor: '#f4f5f7',marginBottom: 10}} level='1'>
                  <Picker
                    selectedValue={idSekolah}
                    style={{height: 40, width: 300}}
                    onValueChange={(itemValue, itemIndex) =>
                      setIdsekolah(itemValue)
                    }>
                      {datasekolah.map((val) =>  {
                        return (
                          <Picker.Item label={val.nama_sekolah} value={val.id_sekolah}/>
                          
                        )
                      }) }
                  </Picker>
              </Layout>

              <Layout style={{backgroundColor: '#f4f5f7',marginBottom: 10}} level='2'>
                    <Picker
                      selectedValue={idKelas}
                      style={{height: 40, width: 300}}
                      onValueChange={(itemValue, itemIndex) =>
                        
                        setIdkelas(itemValue)
                      }>
                        {datakelas.length > 0 ? (
                            datakelas.filter((e) => {
                              if(e.id_sekolah == idSekolah){
                                return e
                              }
                            }).map((val,key) => {
                              return (
                                <Picker.Item label={val.tingkat_kelas+""+val.nama} value={val.id_kelas}/>
                              )
                            })
                        ):(
                          <Picker.Item label="Kosong"/>
                        )}
                    </Picker>
              </Layout>
            
              
            </>

            )}
            
            <Layout level='1'>
                <Button onPress={() => Register()} appearance='outline' status='info'  accessoryLeft={indikator}>Daftar</Button>
            </Layout>
        </Layout>
        </>
    )

}

const styles = StyleSheet.create({
    container: {
      paddingTop: 200,
      minHeight: 158,
      
    },
  });