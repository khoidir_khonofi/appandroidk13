import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View, AsyncStorage,TouchableWithoutFeedback  } from 'react-native';
import { IndexPath, Layout, Input, Text, Icon, Button, Spinner} from '@ui-kitten/components';
import axios from "axios";
import { NavigationActions } from '@react-navigation/compat';
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect, useSelector, useDispatch } from 'react-redux'
import {setUser} from "../../features/UserSlice"


const AppRender = ({route, navigation, setAuth, auth }) => {
  const dispatch = useDispatch();
  const [isSave, setisSave] = useState(false)
  const [isload, setIsload] = useState(false)
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);
  const StarIcon = (props) => (
    <Icon {...props} name='log-in-outline'/>
  );

  const [nama, setNama] = useState("")

  const indikator = (props) => {
    return isload ? (
     <View style={[props.style, styles.indicator]}>
         <Spinner size='large'/>
     </View>                     
       ) : null
   }
  
  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
    </TouchableWithoutFeedback>
  );
  const Login = async () => {
   setisSave(true)
   setIsload(true)
   try {
    
    let response = await axios.post('http://192.168.43.13/tugasakhir/web/index.php?r=api/site/login', {
      username,
      password
    });
    alert('Login Berhasil')
    if(response && response.data){
    

      let auth_ = {
        username:response.data.data.username,
        role:response.data.data.role,
        email: response.data.data.email,
        token:response.data.data.token,
      }
      
      let authdata = JSON.stringify(auth_)
      await AsyncStorage.setItem('auth', authdata);
      
      await setAuth(authdata);
      //dia bakal auto login....
      return 1;
    }
    alert('login gagal')
   } catch (error) {
     
    setIsload(false)
     alert('Username atau password salahh')
   }
  }

  return (
      <>
    
      <View style={{flex: 1, justifyContent: 'center', marginLeft: 30, marginRight: 30}}>
        <Text category='h5' status='info' style={{textAlign: 'center'}}>LOGIN SMART: K13</Text>
     
            <Layout style={{ marginBottom: 20}}>
              <Input 
                placeholder='Username'
                value={username}
                onChangeText={nextValue => setUsername(nextValue)}
              /> 
            </Layout>

            <Layout style={{marginBottom: 20}}>
            <Input
              value={password}
              placeholder='Password'
              accessoryRight={renderIcon}
              secureTextEntry={secureTextEntry}
              onChangeText={nextValue => setPassword(nextValue)}
            />
            </Layout>   
      
          <Layout level='1'>
            <Button onPress={() => Login()} status='info'  appearance='outline' accessoryLeft={indikator}>
              Login
            </Button>
          </Layout>
          <Text style={{paddingTop :10}} onPress={() => navigation.navigate('Daftar')} status='info'>Belum punya akun ?</Text>
          
          </View>
    </>
    
  );
 

};

// conect to redux
const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
      setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AppRender);


const styles = StyleSheet.create({
  container: {
    paddingTop: 200,
    minHeight: 158,
    
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

