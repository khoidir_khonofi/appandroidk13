import { Layout, Text, Avatar, MenuItem,
    Button, Card, Modal,Icon } from '@ui-kitten/components';
  import React, { useState, useEffect } from 'react';
  import axios from 'axios';
  import {View, ActivityIndicator, StyleSheet} from "react-native"
  import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
  const baseUrl =  "http://192.168.43.13/tugasakhir/web/foto_guru/";
  import * as REDUXTYPES from './../../reduxs/type-redux/index'
  import { connect } from 'react-redux';

  const List = ({setData, jenjangpendidikan, auth, data, route, navigation}) => {
    const [visible, setVisible] = React.useState(false);
    const [idClick, setIdClick] = useState(0);
    const [idSiswa, setIdSiswa] = useState(0);
    const [nama, setNama] = useState("");

    const Deletesd = () => {
        axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilai/deleteketerampilan&id="+idClick).then((res) => {
            alert(res.data)
        }).catch((e) => alert(e))
    }
    const Deletesmp = () => {
        axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaismp/deleteketerampilan&id="+idClick).then((res) => {
            alert(res.data)
        }).catch((e) => alert(e))
    }
    const Deletesma = () => {
        axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaisma/deleteketerampilab&id="+idClick).then((res) => {
            alert(res.data)
        }).catch((e) => alert(e))
    }

        return (
          <>
            <FlatList
            data = {data}
            keyExtractor= {(item, index) => {item.id_nilai_harian_keterampilan}}
            renderItem={({item})=>{
              return (
                <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}}>
                  <Layout style={{flexDirection:'row'}}>
                    {jenjangpendidikan === 'SD' ? (
                        <Layout style={{flex:3, marginLeft:5, padding:5}}>
                          <Text>{item.nama_siswa} {item.tingkat_kelas}{item.nama_kelas}</Text>
                          <Text>Mata Pelajaran : {item.nama_mata_pelajaran}</Text>
                          <Text>KD : {item.no_kd} </Text>
                          <Text>Tema : {item.tema}</Text>
                          <Text>Jenis Penilaian : {item.jenis_penilaian}</Text>
                          <Text>Nilai : {item.nilai}</Text>
                          <Text style={{fontSize:10}}>Waktu : {item.created_at}</Text>
                        </Layout>
                    ): jenjangpendidikan === 'SMP' ? (
                      <Layout style={{flex:3, marginLeft:5, padding:5}}>
                        <Text>{item.nama_siswa} {item.tingkat_kelas}{item.nama_kelas}</Text>
                        <Text>Mata Pelajaran : {item.nama_mata_pelajaran}</Text>
                        <Text>KD : {item.no_kd}</Text>
                        <Text>Jenis Penilaian : {item.nama_penilaian}</Text>
                        <Text>Nilai : {item.nilai}</Text>
                        <Text style={{fontSize:10}}>Waktu : {item.created_at}</Text>
                      </Layout>
                    ):(
                      <Layout style={{flex:3, marginLeft:5, padding:5}}>
                        <Text>{item.nama_siswa} {item.tingkat_kelas}{item.nama_kelas}</Text>
                        <Text>Mata Pelajaran : {item.nama_mata_pelajaran}</Text>
                        <Text>KD : {item.no_kd}</Text>
                        <Text>Jenis Penilaian : {item.nama_penilaian}</Text>
                        <Text>Nilai : {item.nilai}</Text>
                        <Text style={{fontSize:10}}>Waktu : {item.created_at}</Text>
                      </Layout>
                    )}
                    
                    <Layout
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                        >
                        <Icon name="more-vertical-outline" fill='#8F9BB3' onPress={() => {
                          setVisible(true);
                          setIdClick(item.id_nilai_harian_keterampilan);
                          setIdSiswa(item.id_siswa)
                          setNama(item.nama_siswa)
                          
                          
                        }} style={{  width: 32,
                          height: 32}}/>
                      </Layout>
                  </Layout>
                </Layout>
              )
            }}
            />
            <Modal 
              visible={visible} 
              backdropStyle={styles.backdrop}
              style={styles.modall}
              onBackdropPress={() => setVisible(false)}>
                  {jenjangpendidikan === 'SD' ? (
                  <>
                    <MenuItem
                      onPress={() =>{
                        setVisible(false)
                        navigation.navigate("Edit Nilai Harian Keterampilan SD",{id : idClick})
                      }
                      }
                      title={"Edit"}
                    />
                     <MenuItem
                      onPress={() =>{
                        setVisible(false)
                        navigation.navigate("Lihat Nilai Perkd Keterampilan",{id_siswa : idSiswa})
                      }
                      }
                      title={"Lihat Nilai per KD"}
                    />
                    <MenuItem
                      onPress={() =>{
                        setVisible(false)
                        Deletesd({idClick})
                      }
                      }
                      title={"Delete"}
                    />
                    </>
                  ): jenjangpendidikan === 'SMP' ? (
                  <>
                  <MenuItem
                    onPress={() =>{
                      setVisible(false)
                      navigation.navigate("Edit Nilai Harian Keterampilan SMP",{id : idClick})
                    }
                    }
                    title={"Edit"}
                  />  
                   <MenuItem
                      onPress={() =>{
                        setVisible(false)
                        navigation.navigate("Lihat Nilai Perkd Keterampilan",{id_siswa : idSiswa})
                      }
                      }
                      title={"Lihat Nilai per KD"}
                    />
                  <MenuItem
                    onPress={() =>{
                      setVisible(false)
                      Deletesmp({idClick})
                    }
                    }
                    title={"Delete"}
                  />
                  </>
                  ):jenjangpendidikan === 'SMA' ? (
                  <>
                  <MenuItem
                    onPress={() =>{
                      setVisible(false)
                      navigation.navigate("Edit Nilai Harian Keterampilan SMA",{id : idClick})
                    }
                    }
                    title={"Edit"}
                  />
                   <MenuItem
                      onPress={() =>{
                        setVisible(false)
                        navigation.navigate("Lihat Nilai Perkd Keterampilan",{id_siswa : idSiswa})
                      }
                      }
                      title={"Lihat Nilai per KD"}
                    />
                  <MenuItem
                    onPress={() =>{
                      setVisible(false)
                      Deletesma({idClick})
                    }
                    }
                    title={"Delete"}
                  />
                  </>
                ):null}
            </Modal>
          </>
        )
      
  }
  function App({route, auth, navigation}){
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
      username:null, role:null,
      email: null,
      token:null};
    const [isLoading, setIsLoading] = useState(false)
    const [data, setData] = useState("")
    const [jenjangpendidikan, setstate] = useState("")
    const getApi= ()=>{
        axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/user/sekolah&token=${token}`)
        .then((res) =>{
          setstate(res.data);
        }).catch((e) =>  console.log(e))
        

          setIsLoading(true)
           axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/mengajar/nilaiharianketerampilanmengajarsd&id="+route.params.id).then((result) => {
           
            setData(result.data);
            setIsLoading(false)
          })

          setIsLoading(true)
          axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/mengajar/nilaiharianketerampilanmengajarsmp&id="+route.params.id).then((result) => {
         
            setData(result.data);
            setIsLoading(false)
          })

          setIsLoading(true)
          axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/mengajar/nilaiharianketerampilanmengajarsma&id="+route.params.id).then((result) => {
         
            setData(result.data);
            setIsLoading(false)
          })
       
      }
       
     
   
    useEffect(() => {
      getApi();
    },[]);

    return !isLoading ? (
      <List data={data} role={role} jenjangpendidikan={jenjangpendidikan} setData = {setData} navigation={navigation}/>
    ):(
      <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="large" color="#0000ff" />
    </View>
    )
    
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    },
    modall: {
      width:200,
      backgroundColor:"#1bcaff",
      color :"red",
      borderRadius : 5,
    },
    backdrop: {
      backgroundColor: 'rgba(0, 0, 0, 0.7)',
    }
  });
  const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(App);