import { Layout, Text, Avatar, MenuItem,
  Button, Card, Modal,Icon } from '@ui-kitten/components';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {View, ActivityIndicator, StyleSheet} from "react-native"
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
const baseUrl =  "http://192.168.43.13/tugasakhir/web/foto_guru/";
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';


const List =({data, nav, role, setIsLoading})=>{
  const [visible, setVisible] = React.useState(false);
  const [idClick, setIdClick] = useState(0);
  const [idkelas , setIdkelas] = useState(0)
  const [status, setStatus] = useState("")
  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/mengajar/delete&id="+idClick).then((res) => {
        alert(res.data)
    }).catch((e) => alert(e))
  }

  const Aktif = () => {
    setIsLoading(true)
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/mengajar/aktif&id="+idClick).then((res) => {
      setIsLoading(false)
      alert(res.data)
      
  }).catch((e) => alert(e))
  }
  const Nonaktif = () => {
    setIsLoading(true)
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/mengajar/nonaktif&id="+idClick).then((res) => {
      alert(res.data)
      setIsLoading(false)
  }).catch((e) => alert(e))
  }

  return(
    <Layout style={{marginHorizontal:5, backgroundColor:'transparent', borderRadius:5}} level='1'>
      <FlatList
        data={data}
        keyExtractor={(item, index) => {Math.random()}}
        renderItem={({item})=>{
          return(
            <Layout style={{marginTop:5, backgroundColor:'#1bcaff'}} level='1' >
              
                <Layout style={{flexDirection:'row'}} level='1' >
                    <Layout style={{padding:15}} level='1'>
                      {item.foto != null ? (
                        <Avatar
                        size='large'

                        source={{ uri: baseUrl+item.foto}} />
                      ):(
                        <Text>no picture</Text>
                      )}
                    
                    </Layout>
                    {item.status === 'Aktif' ? (
                        <TouchableOpacity onPress={() => nav.navigate('Siswa',{id_kelas:item.id_kelas})}>
                        <Layout style={{flex:2, marginLeft:3, padding:3}} level='1'>
                              <Text>{item.nama_guru}</Text>
                              <Text>{item.tingkat_kelas}{item.nama_kelas}</Text>
                              <Text>{item.nama_mata_pelajaran}</Text>
                              <Text style={{color: 'grey', fontSize:12}}>{item.status}</Text>
                        </Layout>
                        </TouchableOpacity>
                    ):(
                      <Layout style={{flex:2, marginLeft:3, padding:3}} level='1'>
                        <Text style={{color: 'grey'}}>{item.nama_guru}</Text>
                        <Text style={{color: 'grey'}}>{item.tingkat_kelas}{item.nama_kelas}</Text>
                        <Text style={{color: 'grey'}}>{item.nama_mata_pelajaran}</Text>
                        <Text style={{color: 'grey'}}>{item.status}</Text>
                      </Layout>
                    )}
                    

                    {role === 'Administrator' || role === 'Guru' ? (
                        <>
                      <Layout
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                        level='1'
                        >
                        <Icon name="more-vertical-outline" fill='#8F9BB3' onPress={() => {
                          setVisible(true);
                          setIdClick(item.id_mengajar);
                          setIdkelas(item.id_kelas)
                          setStatus(item.status)
                          
                          
                        }} style={{  width: 32,
                          height: 32}}/>
                      </Layout>
                      </>
                      ):null}
                </Layout>
             
            </Layout>
          )
        }}
      />
      <Modal 
          visible={visible} 
          backdropStyle={styles.backdrop}
          style={styles.modall}
          onBackdropPress={() => setVisible(false)}>
           
              {role === 'Administrator' ? (
                <>
                {status === 'Aktif' ? (
                  <>
                    <MenuItem
                      onPress={() =>{
                        setVisible(false)
                        nav.navigate("Siswa",{id_kelas: idkelas})
                        
                      }
                      }
                      title={"Lihat Siswa "}
                    />
                  <MenuItem
                    onPress={() =>{
                      setVisible(false)
                      nav.navigate("Lihat Nilai Harian Pengetahuan Mengajar",{id: idkelas})
                      
                    }
                    }
                    
                    title={"Lihat Nilai Pengetahuan "}
                  />
                  <MenuItem
                    onPress={() =>{
                      setVisible(false)
                      nav.navigate("Lihat Nilai Harian Keterampilan Mengajar", { id: idkelas })
                      
                    }
                    }
                    
                    title={"Lihat Nilai Keterampilan "}
                    />
                  </>
                ):null}
                
               

                      <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          Delete({ id: idClick })
                          }
                        }
                        
                        title="Delete"
                      />
                      {status === 'Aktif' ? (
                        <MenuItem
                          onPress={() =>{
                            setVisible(false)
                            Nonaktif({ id: idClick })
                            }
                          }
                          
                          title="Nonaktifkan"
                        />
                      ):(
                        <MenuItem
                          onPress={() =>{
                            setVisible(false)
                            Aktif({ id: idClick })
                            }
                          }
                          
                          title="Aktifkan"
                        />
                      )}

                </>
              ):(
                <>
                {status === 'Aktif' ? (
                    <>
                    <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Siswa",{id_kelas: idkelas})
                          
                        }
                        }
                        
                        title={"Lihat Siswa "}
                      />
                <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Lihat Nilai Harian Pengetahuan Mengajar", { id: idkelas })
                          
                        }
                        }
                        
                        title={"Lihat Nilai Pengetahuan "}
                      />
                      <MenuItem
                        onPress={() =>{
                          setVisible(false)
                          nav.navigate("Lihat Nilai Harian Keterampilan Mengajar", { id: idkelas })
                          
                        }
                        }
                        
                        title={"Lihat Nilai Keterampilan "}
                      />
                    </>
                ):(
                  <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>Telah di nonaktifkan</Text>
                )}
                
                </>

              )}
              
             
        </Modal>
    </Layout>
  )
}

function App({navigation, auth}) {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false)
  let {token, email, role} = auth.login?JSON.parse(auth.login): {
    username:null, role:null,
    email: null,
    token:null};

  const getApi=()=>{
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/mengajar&token="+token).then((result) => {
     
      setData(result.data);
      setIsLoading(true)
    }).catch((e)=> alert(e))
    
  }
 
  useEffect(() => {
    getApi();
  },[]);

  
 
  return isLoading ? (
    <>
      <List nav={navigation} data={data} role={role} setIsLoading={setIsLoading}/>
    </>
  ) : (
    <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="large" color="#0000ff" />
    </View>
  )
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  modall: {
    width:200,
    backgroundColor:"#1bcaff",
    color :"red",
    borderRadius : 5,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  }
});

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);