import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, Button, View } from 'react-native';
import { IndexPath, Layout, Input, Text } from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation}) {
  const [isLoad, setIsLoad] = useState(false)
  const [data, setData] = useState([]);
  const [isSave, setisSave] = useState(false)
  const [role, setRole] = useState("");
  const [status, setStatus] = useState("");
  const [datakelas, setDatakelas] = useState([])
  const [datamapel, setDatamapel] = useState([])
  const [dataguru, setDataguru] = useState([])
  
  
  const [idkelas, setIdkelas] = useState("")
  const [idmapel, setIdmapel] = useState("")
  const [idguru, setIdguru] = useState("")

  useEffect(() => {
    setIsLoad(true)
    axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/user/selectkelas&token=${token}`).then((result) => {
      setDatakelas(result.data)
    }).catch((e) => console.warn(e))

    setIsLoad(true)
    axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/user/selectmapel`).then((result) => {
      setDatamapel(result.data)
    }).catch((e) => console.warn(e))

    setIsLoad(true)
    axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/user/selectguru`).then((result) => {
      setDataguru(result.data)
    }).catch((e) => console.warn(e))
  },[])

  const saveData = () => {
    setisSave(true)
    axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/user/update&id="+route.params.id, {
      id_user: route.params.id,
      email: email,
      username: username,
      password: password,
      role: role, 
      status: status,
    

    }).then((res) => {
setisSave(false)
      alert(res.data);
      navigation.navigate('User')
  }).catch((e) => {alert('erros nyimpan'); setisSave(false)})
  }

  return (
      <>
       
  <Layout style={styles.container} level='1'>
        {isSave ? (
          <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
        ) : null} 
        {isLoad ? (
          <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
        ) : null} 
          <Layout style={{backgroundColor: '#f4f5f7', marginLeft:30, marginRight:30, marginBottom:5}}>
          
              <Picker
                selectedValue={role}
                style={{height: 40, width: 300}}
                onValueChange={(itemValue, itemIndex) =>
                  setRole(itemValue)
                }>
                  <Picker.Item label="Guru" value="Guru"/>
                    <Picker.Item label="Siswa" value="Siswa"/>
                    <Picker.Item label="Administrator" value="Administrator"/>
              </Picker>
            </Layout>

            <Layout style={{backgroundColor: '#f4f5f7', marginLeft:30, marginRight:30, marginBottom:5}}>
              <Picker
                selectedValue={status}
                style={{height: 40, width: 300}}
                onValueChange={(itemValue, itemIndex) =>
                  setStatus(itemValue)
                }>
                  <Picker.Item label="Aktif" value="Aktif"/>
                    <Picker.Item label="Tidak Aktif" value="Tidak Aktif"/>
              </Picker>
            </Layout>   

    <Layout  style={{marginLeft:30, marginRight:30}}>
        <Layout>
          <Input 
            placeholder='Email'
            value={email}
            onChangeText={nextValue => setEmail(nextValue)}
          /> 
        </Layout>

        <Layout>
          <Input 
            placeholder= 'Username'
            value={username}
            onChangeText={nextValue => setUsername(nextValue)}
          /> 
        </Layout>

        <Layout>
          <Input 
            placeholder='password'
            value={password}
            onChangeText={nextValue => setPassword(nextValue)}
          /> 
        </Layout>
  </Layout>
  
<Layout style={{paddingRight: 30, paddingLeft: 30}} level='1'>
            <Button onPress={() => saveData()} title="Simpan"/>

            
              </Layout>
</Layout>
  



    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});