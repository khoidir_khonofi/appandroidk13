import React, {useEffect, useState} from 'react'
import {View,StyleSheet, FlatList,ActivityIndicator, TextInput, Picker, AsyncStorage} from 'react-native'
import axios from 'axios'
import {Icon, Layout, Text, Avatar,  Button,Datepicker, Spinner } from '@ui-kitten/components';
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';

const List = ({data, route, nav, role, namasiswa, setNamasiswa, saveSiswa, isSave, setNis, nis, namaguru, setNamaguru, saveGuru, nip, setNip, idtelegram, setIdtelegram, setJeniskelaminsiswa, jeniskelaminsiswa, setAgamasiswa, agamasiswa, auth, setTanggalahir, tanggalahir}) => {
  
  const baseUrl =  "http://192.168.43.13/tugasakhir/web/foto_siswa/";
  const baseUrlguru =  "http://192.168.43.13/tugasakhir/web/foto_guru/";
  const baseUrl1 =  "http://192.168.43.13/tugasakhir/web/foto_sekolah/";
  const [isload, setisload] = useState(false)
  const CalendarIcon = (props) => (
    <Icon {...props} name='calendar'/>
  );

  const indikator = (props) => {
    return isSave ? (
     <View style={[props.style, styles.indicator]}>
         <Spinner size='large'/>
     </View>                     
       ) : null
   }

 
    return(
        <FlatList
        data = {data}
        keyExtractor = {(item) => {item.id_siswa}}
        renderItem = {({item}) => {
            return (
              role === 'Administrator' ? (
                <View style={{flex: 1,alignItems: 'center'}}>
                  <Layout style={{padding:5}}>
                   
                    <Avatar
                        resizeMode={'cover'}
                        style={{height:100, width: 100}}
                        source={{ uri: baseUrl1+item.foto}} />
                    </Layout>
                    <Text>{item.nama_sekolah}</Text>
                </View>
              ): role === 'Siswa' ? (
                <>
                <Layout  style={{flex: 1,alignItems: 'center', justifyContent: 'center'}}>
                  <Layout style={{padding:5}}>
                    <Avatar
                        style={{height:100, width: 100}}
                        resizeMode={'cover'}

                        source={{ uri: baseUrl+item.foto}} />
                        
                    </Layout>
                    <Text>{item.nama} {item.nis}</Text>
                </Layout>
                <Layout style={{flex: 1,alignItems: 'center', justifyContent: 'center'}}>
                  
                  <Layout style={{marginBottom:5}}>
                      <TextInput
                            style={{ height: 40, borderColor: 'grey', borderWidth: 1, padding: 5, borderRadius: 4, width: 300}}
                            placeholder = 'Nama'
                            onChangeText={text => setNamasiswa(text)}
                            value={namasiswa}
                        />
                  </Layout>
                  <Layout style={{marginBottom:5}}>
                      <TextInput
                            style={{ height: 40, borderColor: 'grey', borderWidth: 1, padding: 5, borderRadius: 4, width: 300}}
                            placeholder = 'Nis'
                            onChangeText={text => setNis(text)}
                            value={nis}
                        />
                  </Layout>
                  <Layout style={{marginBottom:5}}>
                      <TextInput
                            style={{ height: 40, borderColor: 'grey', borderWidth: 1, padding: 5, borderRadius: 4, width: 300}}
                            placeholder = 'Chat id telegram'
                            onChangeText={text => setIdtelegram(text)}
                            value={idtelegram}
                        />
                  </Layout>
                  <Layout style={{borderRadius:5, marginLeft:30, marginRight:30, borderWidth:1, borderColor:'grey', marginBottom: 5}}>
          
                    <Picker
                      selectedValue={jeniskelaminsiswa}
                      style={{height: 40, width: 300}}
                      onValueChange={(itemValue, itemIndex) =>
                        setJeniskelaminsiswa(itemValue)
                      }>
                          <Picker.Item label="Laki-laki" value="Laki-laki"/>
                          <Picker.Item label="Perempuan" value="Perempuan"/>
                    </Picker>
                  </Layout>
                  <Layout style={{borderRadius:5, marginLeft:30, marginRight:30, borderWidth:1, borderColor:'grey'}}>
                    <Picker
                      selectedValue={agamasiswa}
                      style={{height: 40, width: 300}}
                      onValueChange={(itemValue, itemIndex) =>
                        setAgamasiswa(itemValue)
                      }>
                          <Picker.Item label="Islam" value="Islam"/>
                          <Picker.Item label="Kristen" value="Kristen"/>
                          <Picker.Item label="Katolik" value="Katolik"/>
                          <Picker.Item label="Hindu" value="Hindu"/>
                          <Picker.Item label="Budha" value="Budha"/>
                    </Picker>
                </Layout>
                  <Layout style={{marginTop:10}}>
                     <Button onPress={() => saveSiswa()} appearance='outline' status='info' style={{width:300}}  accessoryLeft={indikator}>Simpan</Button>
                  </Layout>
                </Layout>
                </>
              ):(
                <>
                {isSave ? (
                  <Text>Menyimpan...</Text>
                ):null}
                <Layout style={{flex: 1,alignItems: 'center', justifyContent: 'center'}}>
                  <Layout style={{padding:5}}>
                    <Avatar
                        resizeMode={'cover'}
                        style={{height:100, width: 100}}
                        source={{ uri: baseUrlguru+item.foto}} />
                    </Layout>
                    <Text>{item.nama} - {item.nip}</Text>
                </Layout>
                <Layout style={{flex: 1,alignItems: 'center', justifyContent: 'center'}}>
                
                <Layout style={{marginBottom:5}}>
                    <TextInput
                          style={{ height: 40, borderColor: 'grey', borderWidth: 1, padding: 5, borderRadius: 4, width: 300}}
                          placeholder = 'Nama'
                          onChangeText={text => setNamaguru(text)}
                          value={namaguru}
                      />
                </Layout>
                <Layout style={{marginBottom:5}}>
                    <TextInput
                          style={{ height: 40, borderColor: 'grey', borderWidth: 1, padding: 5, borderRadius: 4, width: 300}}
                          placeholder = 'Nip'
                          keyboardType = 'numeric'
                          onChangeText={text => setNip(text)}
                          value={nip}
                      />
                </Layout>
                <Layout style={{borderRadius:5, marginLeft:30, marginRight:30, borderWidth:1, borderColor:'grey', marginBottom: 5}}>
                    <Picker
                      selectedValue={jeniskelaminsiswa}
                      style={{height: 40, width: 300}}
                      onValueChange={(itemValue, itemIndex) =>
                        setJeniskelaminsiswa(itemValue)
                      }>
                          <Picker.Item label="Laki-laki" value="Laki-laki"/>
                          <Picker.Item label="Perempuan" value="Perempuan"/>
                    </Picker>
                  </Layout>
                <Layout style={{marginTop:5}}>
                   <Button onPress={() => saveGuru()} appearance='outline' status='info' style={{width:300}}>Simpan</Button>
                </Layout>
              </Layout>
              </>
              )
               
            )
        }
        }
        />
    )
}

function App({route, navigation, auth}){
    const [data, setData] = useState([])
    const [isSave, setIssave] = useState(false)
    const [idsiswa, setIdsiswa] = useState(0)
    const [idguru, setIdguru] = useState(0)
    const [namasiswa, setNamasiswa] = useState("")
    const [nis, setNis] = useState("")
    const [namaguru, setNamaguru] = useState("")
    const [jeniskelaminsiswa, setJeniskelaminsiswa] = useState("")
    const [agamasiswa, setAgamasiswa] = useState("")
    const [nip, setNip] = useState("") 
    const [idtelegram, setIdtelegram] = useState("")
    const [tanggalahir, setTanggalahir] = useState("")
    const [isloading, setIsloading] = useState(false)
    let {token, email, role} = auth.login?JSON.parse(auth.login): {
      username:null, role:null,
      email: null,
      token:null};
    const getApi=()=>{


        setIsloading(true)
        axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/site/profil&token="+token).then((result) => {
         
          setData(result.data);
          setIsloading(false)
        }).catch((e)=> alert(e))

        if(role == 'Siswa'){
          axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/site/selectsiswa&token="+token).then((result) => {
            setIdsiswa(result.data.id_siswa)
            setNamasiswa(result.data.nama);
            setNis(result.data.nis)
            setIdtelegram(result.data.chat_id_telegram)
            setJeniskelaminsiswa(result.data.jenis_kelamin)
            setAgamasiswa(result.data.agama)
            setTanggalahir(result.data.tanggal_lahir)
            setIsloading(false)
            
          }).catch((e)=> console.log("error siswa"))
        }else{
          axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/site/selectguru&token="+token).then((result) => {
            setIdguru(result.data.id_guru)
            setNamaguru(result.data.nama);
            setNip(result.data.nip)
            setJeniskelaminsiswa(result.data.jenis_kelamin)
            setIsloading(false)
            
          }).catch((e)=> console.log("error guru"))
        }
        

       
        
      }
   
    useEffect(() => {
      getApi();
    },[]);


    const saveSiswa = () => {
      setIssave(true)
      axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/site/updatesiswa&id="+idsiswa,{
        nama : namasiswa,
        nis: nis,
        chat_id_telegram : idtelegram,
        jenis_kelamin : jeniskelaminsiswa,
        agama : agamasiswa,
        tanggal_lahir : tanggalahir,
      }).then((res) => {
        setIssave(false)
        alert(res.data)
        
      }).catch((e) => alert(e),setIssave(false))
    }

    const saveGuru = () => {
      setIssave(true)
      axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/site/updateguru&id="+idguru,{
        nama : namaguru,
        nip: nip,
        jenis_kelamin : jeniskelaminsiswa,
      }).then((res) => {
        setIssave(false)
        alert(res.data)
        
      }).catch((e) => alert(e),setIssave(false))
    }
    return !isloading ? (
        <List nav = {navigation} data={data} role={role} namasiswa={namasiswa} setNamasiswa={setNamasiswa} saveSiswa={saveSiswa} isSave={isSave} nis={nis} setNis={setNis} saveGuru={saveGuru} setNamaguru={setNamaguru} namaguru={namaguru} setNip={setNip} nip={nip} idtelegram={idtelegram} setIdtelegram={setIdtelegram} jeniskelaminsiswa={jeniskelaminsiswa} setJeniskelaminsiswa={setJeniskelaminsiswa} agamasiswa={agamasiswa} setAgamasiswa={setAgamasiswa} tanggalahir={tanggalahir} setTanggalahir={setTanggalahir}/>
    ): (
        <View style={[styles.container, styles.horizontal]}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
    )
}

const styles = StyleSheet.create({
    container: {
      paddingTop: 200,
      minHeight: 158,
      
    },
  });

  
  const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(App);
