import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  
  ActivityIndicator,
  
  View
} from "react-native";
import {
  Icon,
  Layout,
  Text,
  MenuItem,
  Button, Card, Modal 
} from "@ui-kitten/components";
import axios from "axios";
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';


function App({ navigation, route, auth}) {
  const [data, setData] = useState([]);
  const [visible, setVisible] = React.useState(false);
  const [idClick, setIdClick] = useState(0);
  const [idsiswa, setIdsiswa] = useState(0)
  let {token, email, role} = auth.login?JSON.parse(auth.login): {
    username:null, role:null,
    email: null,
    token:null};

  const [isLoading, setIsLoading] = useState(false);
  const Delete = () => {
    axios.delete("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaikd/deletekdakhir&id="+idClick).then((respon) => {
        alert(respon.data);
    }).catch((e) => console.log(e))
  }
  const getApi = () => {
    axios
      .get(
        "http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaikd/indexpengetahuanakhir&id=" +
          route.params.id_siswa
      )
      .then(result => {
        setData(result.data);
        setIsLoading(true);
      })
      .catch(e => alert("Error Api"));
  };

  useEffect(() => {
    getApi();
  }, [data]);

  

  return isLoading ? (
    <Layout
    
      style={{
        marginHorizontal: 5,
        backgroundColor: "transparent",
        borderRadius: 5
      }}
    >
      {data.map((item, index) => {
        return (
          <Layout
            style={{ marginTop: 5, backgroundColor: "#1bcaff" }}
            key={index}
          >
            <Layout style={{ flexDirection: "row" }}>
              <Layout style={{ flex: 3, marginLeft: 3 }}>
                <Text>{item.siswa.nama}</Text>
                <Text>Mata Pelajaran : {item.mapel.nama_mata_pelajaran}</Text>
                <Text>Total Nilai KD : {item.nilai.nilai_kd}</Text>
                <Text>Npts : {item.nilai.npts}</Text>
                <Text>Npas : {item.nilai.npas}</Text>
                <Text>Nilai Akhir : {item.nilai.nilai_akhir_kd}</Text>
                
              </Layout>


              <Layout
            
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  alignItems: "center",
                }}
              >
              <Icon name="more-vertical-outline" fill='#8F9BB3' onPress={() => {
                setVisible(true);
                setIdClick(item.nilai.id);
                setIdsiswa(item.siswa.id_siswa)
                
              }} style={{  width: 32,
                height: 32}}/>
        
        

                  

              </Layout>
            </Layout>
          </Layout>
        );
      })}
      <Modal 
                  visible={visible} 
                  backdropStyle={styles.backdrop}
                  style={{width: 200, backgroundColor: '#1bcaff'}}
                  onBackdropPress={() => setVisible(false)}>
                        {role === 'Administrator' || role === 'Guru' ? (
                          <>
                          <MenuItem
                                onPress={() =>{
                                  setVisible(false)
                                  navigation.navigate("Input Nilai Npas", { id: idClick })
                                  
                                }
                                }
                                
                                title="Npts dan Npas"
                              />
                               <MenuItem
                                onPress={() =>{
                                  setVisible(false)
                                  navigation.navigate("Lihat Nilai Akhir Detail", { id: idsiswa })
                                  
                                }
                                }
                                
                                title="Lihat nilai akhir"
                              />
                              <MenuItem
                                onPress={() =>{

                                  setVisible(false)
                                  Delete({ id: idClick })
                                  }
                                }
                                
                                title="Delete"
                              />
                          </>
                        ):(
                          <MenuItem
                          onPress={() =>{
                            setVisible(false)
                            navigation.navigate("Lihat Nilai Akhir Detail", { id: idsiswa })
                            
                          }
                          }
                          
                          title="Lihat nilai akhir"
                        />
                        )}
                              
                </Modal>
    </Layout>
  ) : (
    <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="large" color="#0000ff" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  
});

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
