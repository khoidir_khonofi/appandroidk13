import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View ,TextInput} from 'react-native';
import { IndexPath, Layout, Input, Text, Button, Icon, Spinner } from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation}) {
  const [isLoad, setIsLoad] = useState(false)
  const [isSave, setisSave] = useState(false)
  
  
  const [getnpts, setNpts] = useState("")
  const [getnpas, setNpas] = useState("")
  const indikator = (props) => {
    return isSave ? (
     <View style={[props.style, styles.indicator]}>
         <Spinner size='large'/>
     </View>                     
       ) : null
   }
  useEffect(() => {
    axios.get(`http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaikd/select&id=${route.params.id}`).then((result) => {
      setNpts(result.data.npts.toString())
      setNpas(result.data.npas.toString())
       setIsLoad(true)

      
    }).catch((e) => console.warn(e))
  }, [])
  const saveData = () => {
    setisSave(true)
    axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaikd/nilainpts&id="+route.params.id, {
      id_nilai_kd_pengetahuan_akhir: route.params.id,  
      npts: parseFloat(getnpts),
      npas: parseFloat(getnpas), 
    

    }).then((res) => {
      setisSave(false)
      alert(res.data);
  }).catch((e) => {console.log('error', e); setisSave(false)})
  }

 
  return (
      <>
    <Layout style={styles.container} level='1'>
    
      <Layout style={{ paddingRight: 30, paddingLeft: 30}}>
      <Text style={{fontSize: 20, textAlign: "center", marginBottom:10, borderBottomColor: "lightgrey"}}>Input Nilai </Text> 
    
        {isLoad ? (
          <>
          <Layout style={{backgroundColor: "#fbffff", marginTop: 20, borderRadius: 4}}>
          <TextInput
            style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 5, borderRadius: 4 }}
            onChangeText={text => setNpts(text)}
            label = 'sss'
            keyboardType = 'numeric'
            value={getnpts}
          />
          </Layout>

          <Layout style={{backgroundColor: "#fbffff", marginTop: 20, borderRadius: 4}}>
          <TextInput
            style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 5, borderRadius: 4}}
            onChangeText={text => setNpas(text)}
            label = 'df'
            keyboardType = 'numeric'
            value={getnpas}
          />
          </Layout>
          </>
          ): (
            <View style={[styles.container, styles.horizontal]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          )}
            
</Layout>
 
        <Layout style={{marginRight: 30, marginLeft: 30, marginTop: 20}} level='1'>
            <Button onPress={() => saveData()} appearance='outline' status='info' style={{width:300}} accessoryLeft={indikator}>Simpan</Button>
        </Layout>
</Layout>
 



    </>
  );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    }
  });