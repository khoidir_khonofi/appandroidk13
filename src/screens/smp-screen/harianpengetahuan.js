import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View } from 'react-native';
import { IndexPath, Layout, Input, Text , Button,Spinner} from '@ui-kitten/components';
import axios from "axios";
import * as REDUXTYPES from './../../reduxs/type-redux/index'
import { connect } from 'react-redux';

function App ({route, auth}) {
  const [dataMapel, setDataMapel] = useState([])
  const [dataKd, setDataKd] = useState([])
  const [state, setstate] = useState([])
  const [isLoad, setIsLoad] = useState(false)
  const [selectedValue, setSelectedValue] = React.useState(0);
  const [getValKd, setGetValKd] = useState(0)
  const [isSave, setisSave] = useState(false)
  let {token, email, role} = auth?JSON.parse(auth.login):'';
  
  const [nilai, setNilai] = useState("")
  const [nama_penilaian, setNamapenilaian] = useState("")
  const indikator = (props) => {
    return isSave ? (
     <View style={[props.style, styles.indicator]}>
         <Spinner size='large'/>
     </View>                     
       ) : null
   }

  useEffect(() => {
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaismp/mapel&id="+route.params.id).then((result) => {
    
    setIsLoad(true);

      setDataMapel(result.data.datamapel);
      setstate(result.data.Tingkatkelas)
    }).catch((e)=> console.log(e))


    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaismp/kade").then((result) => {
      
  
  
        setDataKd(result.data);
      }).catch((e)=> alert("Error KD"))

  }, [])

  const saveData = () => {
    setisSave(true)
    axios.post("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaismp/smpinputnilaiharianpengetahuan", {
      id_siswa: route.params.id,
      id_kd_pengetahuan: getValKd,
      id_mapel: selectedValue,
      nama_penilaian: nama_penilaian,
      nilai: nilai, 
    

    }).then((res) => {
setisSave(false)
      alert(res.data);
  }).catch((e) => {alert("Eorror"); setisSave(false)})
  }






  return (
      <>
    <Layout style={styles.container} level='1'>
    
      <Layout style={{paddingRight: 30, paddingLeft: 30}}>
      <Text style={{fontSize: 20, textAlign: "center", marginBottom:10, borderBottomColor: "lightgrey"}}>Input Nilai Pengetahuan</Text> 
      
          <Layout style={{marginBottom: 5, borderWidth: 1, borderColor: 'grey', borderRadius: 5}}>
       
            <Picker
              selectedValue={selectedValue}
              style={{borderBottomColor: "#000", borderBottomWidth: 5}}
              onValueChange={(value, index) => setSelectedValue(value)}
              >
              {dataMapel.map((val, key) =>  {
                return (
                  <Picker.Item label={val.nama_mata_pelajaran} value={val.id_mapel}/>
                      
                )
              }) }
            </Picker>
          </Layout>

          <Layout style={{marginBottom: 5, borderWidth: 1, borderColor: 'grey', borderRadius: 5}}>
            <Picker
              selectedValue={getValKd}
              onValueChange={(value, index) => setGetValKd(value)}
            > 
              {dataKd.length > 0 ? (
                dataKd.filter((e) => {
                  if(e.id_mapel==selectedValue){
                    return e
                  }
                  
                }).map((val, key) => {
                  return (

                    <Picker.Item label={val.no_kd} value={val.id_kd_pengetahuan}/>
                        
                  )
                }) 

              ) : (
                <Picker.Item label="Data tidak tersedia"/>
              )}
            
            </Picker>
          </Layout>

        <Layout>
          <Input 
            placeholder='Nama Penilaian'
            value={nama_penilaian}
            onChangeText={nextValue => setNamapenilaian(nextValue)}
          /> 
          </Layout>

          <Layout>
          <Input 
            placeholder='Nilai'
            value={nilai}
            keyboardType = 'numeric'
            onChangeText={nextValue => setNilai(nextValue)}
          /> 
          </Layout>

            
</Layout>
<Layout style={{paddingRight: 30, paddingLeft: 30}} level='1'>
    <Button onPress={() => saveData()} appearance='outline' status='info'  accessoryLeft={indikator}>Simpan</Button>
</Layout>
</Layout>

  




    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      setAuth: (payload) => dispatch({ type: REDUXTYPES.AUTH_LOGIN, payload })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);