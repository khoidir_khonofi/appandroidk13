import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View, TextInput } from 'react-native';
import { IndexPath, Layout, Input, Text ,Button, Spinner} from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation}) {
  const [isSave, setisSave] = useState(false)
  const [Isload, setIsLoad] = useState(false)
  const [nama, setNama] = useState("")
  const [nilai, setNilai] = useState("")
  const indikator = (props) => {
    return isSave ? (
     <View style={[props.style, styles.indicator]}>
         <Spinner size='large'/>
     </View>                     
       ) : null
   }

  useEffect(() => {
    setIsLoad(true);
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaismp/selectketerampilan&id="+route.params.id).then((result) => {
      setNama(result.data.data.nama_siswa)
      setNilai(result.data.data.nilai.toString())
      setIsLoad(false);
    }).catch((e)=> console.log(e))
  }, [])

  const saveData = () => {
    setisSave(true)
    axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaismp/updateketerampilan&id="+route.params.id, {
      id_nilai_harian_keterampilan: route.params.id,
      nilai: parseFloat(nilai), 
    

    }).then((res) => {
setisSave(false)
      alert(res.data);
  }).catch((e) => {alert(e); setisSave(false)})
  }


  return (
      <>
     
     
        <Layout style={styles.container} level='1'>
        <Text style={{textAlign: 'center', marginBottom: 20}}>Edit {nama}</Text>
            <Layout style={{paddingRight: 30, paddingLeft: 30}}>
                <Layout level="1">
                
                {Isload ? (
                    <View style={[styles.container, styles.horizontal]}>
                    <ActivityIndicator size="large" color="#0000ff" />
                  </View>
                ): null} 
                <TextInput
                   style={{ height: 40, marginTop: 10,  borderColor: 'gray', borderWidth: 1, padding: 5, borderRadius: 4}}
                   onChangeText={text => setNilai(text)}
                   keyboardType = 'number-pad'
                   value={nilai}
               />
               </Layout>
           
        </Layout>
        <Layout style={{paddingRight: 30, paddingLeft: 30, marginTop :10}}>
               
                <Button onPress={() => saveData()}   appearance='outline' status='info' accessoryLeft={indikator}>
                Simpan</Button>
            </Layout>
        </Layout>
        
       
        
    </>
    
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});