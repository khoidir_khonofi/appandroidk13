import React, {useEffect, useState} from 'react';
import { StyleSheet, Picker, ActivityIndicator, View, TextInput } from 'react-native';
import { IndexPath, Layout, Input, Text, Button, Spinner } from '@ui-kitten/components';
import axios from "axios";

export default function App ({route, navigation}) {
  const [namapenilaian, setNamapenilaian] = useState("")
  const [isSave, setisSave] = useState(false)
  const [Isload, setIsLoad] = useState(false)
  const [nama, setNama] = useState("")
  const [nilai, setNilai] = useState("")
  const indikator = (props) => {
    return isSave ? (
     <View style={[props.style, styles.indicator]}>
         <Spinner size='large'/>
     </View>                     
       ) : null
   }

  useEffect(() => {
    setIsLoad(true);
    axios.get("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaismp/selectpengetahuan&id="+route.params.id).then((result) => {
    
    
      setNama(result.data.data.nama_siswa)
      setNamapenilaian(result.data.data.nama_penilaian)
      setNilai(result.data.data.nilai.toString())
      setIsLoad(false);
      
    }).catch((e)=> console.log(e))
  }, [])

  const saveData = () => {
    setisSave(true)
    axios.put("http://192.168.43.13/tugasakhir/web/index.php?r=api/nilaismp/updatepengetahuan&id="+route.params.id, {
      id_nilai_harian_pengetahuan: route.params.id,
      nama_penilaian: namapenilaian,
      nilai: parseFloat(nilai), 
    

    }).then((res) => {
setisSave(false)
      alert(res.data);
  }).catch((e) => {alert(e); setisSave(false)})
  }


  return (
      <>
    
    

        <Layout style={styles.container} level='1'>
        <Text style={{textAlign: 'center', marginBottom: 20}}>Edit {nama}</Text>
           
            {Isload ? (
                <View style={[styles.container, styles.horizontal]}>
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
            ): null}
            <Layout style={{paddingRight: 30, paddingLeft: 30}}>
                <Layout level="1">
                
                 <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 5, borderRadius: 4}}
                    onChangeText={text => setNamapenilaian(text)}
                    value={namapenilaian}
                />
                </Layout>
                <Layout level="1">
                
                <TextInput
                   style={{ height: 40, marginTop: 10,  borderColor: 'gray', borderWidth: 1, padding: 5, borderRadius: 4}}
                   onChangeText={text => setNilai(text)}
                   keyboardType = 'number-pad'
                   value={nilai}
               />
               </Layout>
           
        </Layout>
        <Layout style={{ paddingRight: 30, paddingLeft: 30, marginTop:10}}>
          <Button onPress={() => saveData()}   appearance='outline' status='info' accessoryLeft={indikator}>
                Simpan</Button>
            </Layout>
        </Layout>
        
    </>
    
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});