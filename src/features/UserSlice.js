import { createSlice } from '@reduxjs/toolkit'

initialState ={
    username: null,
    role: null,
    token: null,
    email: null
}
const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action) => {
     state.username = action.payload.username
     state.role = action.payload.role
     state.token = action.payload.token
     state.email = action.payload.email
    },
    removeUser: state => {
        state.username = null
        state.role = null
        state.token = null
        state.email = null
    }
  }
})

export const { setUser, removeUser } = userSlice.actions
export const selectRole = state => state.user.role;
export const selectToken = state => state.user.token;
export const selectUsername = state => state.user.username;

export default userSlice.reducer