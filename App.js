/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { mainStore as store } from './src/reduxs/stores-redux/index';
import { Provider } from 'react-redux'
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import * as eva from '@eva-design/eva';
import {RenderPrimaryNavigator} from './src/navigators/index'



const App = () => {

  return (

    <SafeAreaProvider>
      <StatusBar barStyle="light-content" />
            <IconRegistry icons={EvaIconsPack} />
            <Provider store={store}>
                <ApplicationProvider {...eva} theme={{ ...eva.light }}>
                <RenderPrimaryNavigator />
                </ApplicationProvider>
            </Provider>
      </SafeAreaProvider>

  );
};

const styles = StyleSheet.create({
  
});

export default App;
